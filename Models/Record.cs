﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace skiShop.Models
{
    public class Record
    {
        public int Id { get; set; }
        [Required]
        public DateTime LogTime { get; set; }
        [Required]
        public string FullName { get; set; }
        [Required]
        public string Phone { get; set; }
        public string Address { get; set; }
        public string EmailAddress { get; set; }
        public string LocalAccommodations { get; set; }
        public string HeightWeight { get; set; }
        public string AgeSex { get; set; }
        public string CustomerNotes { get; set; }
        public string SkierType { get; set; }
        public string SkiBrand { get; set; }
        [Display(Name = "Ski Size")]
        public string SkiSize { get; set; }
        public string SkiInventoryNumber { get; set; }
        [Display(Name = "Board Brand")]
        public string BoardBrand { get; set; }
        [Display(Name = "Board Stance")]
        public string BoardStance { get; set; }
        [Display(Name = "Board Size")]
        public string BoardSize { get; set; }
        public string SnowboardInventoryNumber { get; set; }
        [Display(Name = "Stance Angles")]
        public string StanceAngles { get; set; }
        [Display(Name = "Pole Size")]
        public string PoleSize { get; set; }
        [Display(Name = "Pole Brand")]
        public string PoleBrand { get; set; }
        public string PoleInventoryNumber { get; set; }
        [Display(Name = "Binding Size")]
        public string BindingSize { get; set; }
        [Display(Name = "Binding Brand")]
        public string BindingBrand { get; set; }
        public string SkiBindingInventoryNumber { get; set; }
        public bool PublicSubmission { get; set; }
        public string BootSize { get; set; }
        public string BootSoleLength { get; set; }
        public string BootBrand { get; set; }
        public string BootInventoryNumber { get; set; }
        public string SnowboardBindingBrand { get; set; }
        public string SnowboardBindingSize { get; set; }
        public string SnowboardBindingInventoryNumber { get; set; }
        public string InventoryNumber { get; set; }
        public string DIN { get; set; }
        public bool Returned { get; set; }
    }
}
