﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace skiShop.Models
{
    public class ShopPasswordViewModel
    {
        public string OldPassword { get; set; }
        public string Password { get; set; }
    }
}
