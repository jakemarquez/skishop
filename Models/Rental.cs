﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace skiShop.Models
{
    public class Rental
    {
        public int ID { get; set; }
        [Display(Name = "First Name")]
        [Required]
        public string FirstName { get; set; }
        [Display(Name ="Last Name")]
        public string LastName { get; set; }
        public string Address { get; set; }
        public string City { get; set; } 
        public string State { get; set; }
        [Display(Name = "Zip")]
        public string PostalCode { get; set; }
        [Display(Name ="Phone Number")]
        public double PhoneNumber { get; set; }
        [Display(Name ="Alt Phone")]
        public double PhoneNumber2 { get; set; }
        [Display(Name ="Email Address")]
        public string EmailAddress { get; set; }
        [Display(Name ="Local Accommodations")]
        public string LocalAccommodations { get; set; }
        public string Height { get; set; }
        public int Weight { get; set; }
        [Display(Name ="Skier Type")]
        public string SkierType { get; set; }
        [Display(Name ="Ski Brand")]
        public string SkiBrand { get; set; }
        [Display(Name ="Ski Size")]
        public string SkiSize { get; set; }
        [Display(Name ="Board Brand")]
        public string BoardBrand { get; set; }
        [Display(Name ="Board Stance")]
        public string BoardStance { get; set; }
        [Display(Name ="Board Size")]
        public string BoardSize { get; set; }
        [Display(Name ="Stance Angles")]
        public string StanceAngles { get; set; }
        [Display(Name ="Pole Size")]
        public string PoleSize { get; set; }
        [Display(Name ="Pole Brand")]
        public string PoleBrand { get; set; }
        [Display(Name ="Binding Size")]
        public string BindingSize { get; set; }
        [Display(Name ="Binding Brand")]
        public string BindingBrand { get; set; }
        [Display(Name = "Snowboard Binding Size")]
        public string SnowboardBindingSize { get; set; }
        [Display(Name = "Snowboard Binding Brand")]
        public string SnowboardBindingBrand { get; set; }
        public int Age { get; set; }
        public string Sex { get; set; }
        [Display(Name ="Customer Notes")]
        public string CustomerNotes { get; set; }
        [Display(Name = "Returned")]
        public bool Returned { get; set; }
        public bool PartialReturn { get; set; }
        public string PartialReturnMessage { get; set; }
        public List<Record> Records { get; set;}
        public bool PublicSubmission { get; set; }
        [Display(Name = "Boot Brand")]
        public string BootBrand { get; set; }
        [Display(Name = "Boot Sole Length")]
        public string BootSoleLength { get; set; }
        [Display(Name = "Boot Size")]
        public string BootSize { get; set; }
        [Display(Name = "Inventory Number")]
        public string InventoryNumber { get; set; }
        [Display(Name = "Snowboard Inventory Number")]
        public string SnowboardInventoryNumber { get; set; }
        [Display(Name = "Ski Inventory Number")]
        public string SkiInventoryNumber { get; set; }
        [Display(Name = "Pole Inventory Number")]
        public string PoleInventoryNumber { get; set; }
        [Display(Name = "Ski Binding Inventory Number")]
        public string SkiBindingInventoryNumber { get; set; }
        [Display(Name = "Boots Inventory Number")]
        public string BootsInventoryNumber { get; set; }
        [Display(Name = "Snowboard Binding Inventory Number")]
        public string SnowboardBindingInventoryNumber { get; set; }
        [Display(Name = "DIN")]
        public string DIN { get; set; }
        [Display(Name = "Date Created")]
        public DateTime DateCreated { get; set; }
        public string IdNext { get; set; }
        public string IdPrevious { get; set; }
        [Display(Name = "Daily Rental")]
        public bool DailyRental { get; set; }
        public bool IsMetric { get; set; } 
        [Display(Name = "Due Date")]
        public DateTime? DueDate { get; set; }
        public Rental()
        {
            this.Records = new List<Models.Record>();
        }
    }
}
