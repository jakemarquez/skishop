﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace skiShop.Models
{
    public class RentalReportViewModel
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public double PhoneNumber { get; set; }
        public bool DailyRental { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DueDate { get; set; }
    }
}
