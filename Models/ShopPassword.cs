﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace skiShop.Models
{
    public class ShopPassword
    {
        public int ID { get; set; }
        [DataType(DataType.Password)]
        [Required]
        public string Password { get; set; }
        public ShopPassword()
        {
            Password = "Password_1";
        }
    }
}
