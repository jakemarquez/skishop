﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace skiShop.Models
{
    public class RentalViewModel4
    {
        public int ID { get; set; }
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Display(Name ="Last Name")]
        public string LastName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        [Display(Name ="Phone Number")]
        public double PhoneNumber { get; set; }
        [Display(Name ="Alt Phone")]
        public double PhoneNumber2 { get; set; }
        [Display(Name ="Email Address")]
        public string EmailAddress { get; set; }
        [Display(Name = "Returned")]
        public bool Returned { get; set; }
        public bool PartialReturn { get; set; }
        [Display(Name = "Date Created")]
        public DateTime DateCreated { get; set; }
        [Display(Name = "Daily Rental")]
        public bool DailyRental { get; set; }
        [Display(Name = "Due Date")]
        public DateTime? DueDate { get; set; }
    }
}
