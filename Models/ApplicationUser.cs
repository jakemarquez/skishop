﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace skiShop.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
         /// <summary>
         /// Use Text Suggestions ?
         /// </summary>
        public bool TextSuggestions { get; set; }
        
        /// <summary>
        /// Should Text all be capitalized?
        /// </summary>
        public bool Capitalize { get; set; }

        /// <summary>
        /// Should the website be displayed in large font?
        /// </summary>
        public bool LargeFont { get; set; }
        
    }
}
