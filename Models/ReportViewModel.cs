﻿using System.Collections.Generic;

namespace skiShop.Models
{
    public class ReportViewModel
    {
        public ReportViewModel()
        {
            this.ToBeReturned = new List<RentalReportViewModel>();
        }
        public int BindingRentals { get; set; }
        public int BootRentals { get; set; }
        public int CurrentDailyReturned { get; set; }
        public int CurrentDailyUnreturned { get; set; }
        public int DailyCount { get; set; }
        public int DailyReturned { get; set; }
        public int DailyUnreturned { get; set; }
        public string TopPoleRental { get; set; }
        public int PoleRentals { get; set; }
        public int RentalCount { get; set; }
        public int ReturnedCount { get; set; }
        public int SeasonalCount { get; set; }
        public int SkiRentals { get; set; }
        public int SnowboardRentals { get; set; }
        public string TopBindingRental { get; set; }
        public int TopBindingRentalCount { get; set; }
        public string TopBootRental { get; set; }
        public int TopBootRentalCount { get; set; }
        public int TopPoleRentalCount { get; set; }
        public string TopSkiRental { get; set; }
        public int TopSkiRentalCount { get; set; }
        public string TopSnowboardRental { get; set; }
        public int TopSnowboardRentalCount { get; set; }
        public int UnreturnedCount { get; set; }
        public int UserCount { get; set; }
        public int PartiallyReturnedCount { get; set; }
        public List<RentalReportViewModel> ToBeReturned { get; set; }
    }
}