﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace skiShop.Models
{
    public class skiShopContext : IdentityDbContext<ApplicationUser>
    {
        public skiShopContext (DbContextOptions<skiShopContext> options)
            : base(options)
        {
        }

        public DbSet<Rental> Rental { get; set; }
        public DbSet<ShopPassword> ShopPassword { get; set; }

        public DbSet<Record> Records { get; set; }

        public DbSet<AdminOption> AdminOptions { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {

            base.OnModelCreating(builder);
           
        }
    }
}
