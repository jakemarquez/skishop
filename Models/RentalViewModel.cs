﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace skiShop.Models
{
    public class RentalViewModel
    {
        public Rental Rental { get; set;}

        public Rental RentalOld {get;set;}

        public bool Save { get; set; }
    }
}
