REM download the latest version of the app
git pull origin master
REM set the environment to Backup
SET ASPNETCORE_ENVIRONMENT=Backup
REM launch browser
start chrome "http://localhost:5000"
REM clear cmd window and launch
prompt $G
CLS
@ECHO OFF
ECHO **********     Starting Rental Logistics Offline mode!     **********
ECHO The website will now be run from this computer in the background
ECHO Close this window to stop running the website
dotnet run > localstartup_logs.txt
