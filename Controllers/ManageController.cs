﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using skiShop.Models;
using skiShop.Models.ManageViewModels;
using skiShop.Services;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.Text;
using System.Text.RegularExpressions;

namespace skiShop.Controllers
{
    [Authorize]
    public class ManageController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IEmailSender _emailSender;
        private readonly ISmsSender _smsSender;
        private readonly ILogger _logger;
        private readonly skiShopContext _context;

        public ManageController(
        UserManager<ApplicationUser> userManager,
        SignInManager<ApplicationUser> signInManager,
        IEmailSender emailSender,
        ISmsSender smsSender,
        ILoggerFactory loggerFactory,
        skiShopContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _smsSender = smsSender;
            _logger = loggerFactory.CreateLogger<ManageController>();
            _context = context;
        }

        //
        // GET: /Manage/Index
        [HttpGet]
        public async Task<IActionResult> Index(ManageMessageId? message = null, string subMessage = null)
        {
            ViewData["StatusMessage"] =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.SetTwoFactorSuccess ? "Your two-factor authentication provider has been set."
                : message == ManageMessageId.Error ? "An error has occurred."
                : message == ManageMessageId.AddPhoneSuccess ? "Your phone number was added."
                : message == ManageMessageId.RemovePhoneSuccess ? "Your phone number was removed."
                : message == ManageMessageId.ChangeShopPasswordSuccess ? "The Shop Password has been changed."
                : message == ManageMessageId.ToggleShopPasswordLock ? "The Shop Password Lock has been Toggled"
                : "";
            if (subMessage == "ShopPasswordLock") {
                CookieOptions cookieOptions = new CookieOptions();
                Response.Cookies.Append("settingChanged", "Password", cookieOptions);
            }
            if (subMessage == "Form"){
                CookieOptions cookieOptions = new CookieOptions();
                Response.Cookies.Append("settingChanged", "Form", cookieOptions);
            }
            else if (subMessage != null) { ViewData["StatusMessage"] = subMessage; }
            var user = await GetCurrentUserAsync();
            ViewData["TextSuggestions"] = user.TextSuggestions;
            ViewData["CapitalizeWords"] = user.Capitalize;
            ViewData["LargeFont"] = user.LargeFont;
            ViewData["ShopPassword"] = (from d in _context.AdminOptions where d.Name == "ShopPasswordLocked" select d).First().Value; 
            ViewData["CreateForm"] = (from d in _context.AdminOptions where d.Name == "CreateForm" select d).First().Value;
            if (user == null)
            {
                return View("Error");
            }
            var model = new IndexViewModel
            {
                HasPassword = await _userManager.HasPasswordAsync(user),
                PhoneNumber = await _userManager.GetPhoneNumberAsync(user),
                TwoFactor = await _userManager.GetTwoFactorEnabledAsync(user),
                Logins = await _userManager.GetLoginsAsync(user),
                BrowserRemembered = await _signInManager.IsTwoFactorClientRememberedAsync(user)
            };
            return View(model);
        }

        //
        // POST: /Manage/RemoveLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RemoveLogin(RemoveLoginViewModel account)
        {
            ManageMessageId? message = ManageMessageId.Error;
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                var result = await _userManager.RemoveLoginAsync(user, account.LoginProvider, account.ProviderKey);
                if (result.Succeeded)
                {
                    await _signInManager.SignInAsync(user, isPersistent: false);
                    message = ManageMessageId.RemoveLoginSuccess;
                }
            }
            return RedirectToAction(nameof(ManageLogins), new { Message = message });
        }

        //
        // GET: /Manage/AddPhoneNumber
        public IActionResult AddPhoneNumber()
        {
            return View();
        }

        //
        // POST: /Manage/AddPhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddPhoneNumber(AddPhoneNumberViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            // Generate the token and send it
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return View("Error");
            }
            var code = await _userManager.GenerateChangePhoneNumberTokenAsync(user, model.PhoneNumber);
            await _smsSender.SendSmsAsync(model.PhoneNumber, "Your security code is: " + code);
            return RedirectToAction(nameof(VerifyPhoneNumber), new { PhoneNumber = model.PhoneNumber });
        }

        //
        // POST: /Manage/EnableTwoFactorAuthentication
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EnableTwoFactorAuthentication()
        {
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                await _userManager.SetTwoFactorEnabledAsync(user, true);
                await _signInManager.SignInAsync(user, isPersistent: false);
                _logger.LogInformation(1, "User enabled two-factor authentication.");
            }
            return RedirectToAction(nameof(Index), "Manage");
        }

        //
        // POST: /Manage/DisableTwoFactorAuthentication
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DisableTwoFactorAuthentication()
        {
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                await _userManager.SetTwoFactorEnabledAsync(user, false);
                await _signInManager.SignInAsync(user, isPersistent: false);
                _logger.LogInformation(2, "User disabled two-factor authentication.");
            }
            return RedirectToAction(nameof(Index), "Manage");
        }

        //
        // GET: /Manage/VerifyPhoneNumber
        [HttpGet]
        public async Task<IActionResult> VerifyPhoneNumber(string phoneNumber)
        {
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return View("Error");
            }
            var code = await _userManager.GenerateChangePhoneNumberTokenAsync(user, phoneNumber);
            // Send an SMS to verify the phone number
            return phoneNumber == null ? View("Error") : View(new VerifyPhoneNumberViewModel { PhoneNumber = phoneNumber });
        }

        //
        // POST: /Manage/VerifyPhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> VerifyPhoneNumber(VerifyPhoneNumberViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                var result = await _userManager.ChangePhoneNumberAsync(user, model.PhoneNumber, model.Code);
                if (result.Succeeded)
                {
                    await _signInManager.SignInAsync(user, isPersistent: false);
                    return RedirectToAction(nameof(Index), new { Message = ManageMessageId.AddPhoneSuccess });
                }
            }
            // If we got this far, something failed, redisplay the form
            ModelState.AddModelError(string.Empty, "Failed to verify phone number");
            return View(model);
        }

        //
        // POST: /Manage/RemovePhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RemovePhoneNumber()
        {
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                var result = await _userManager.SetPhoneNumberAsync(user, null);
                if (result.Succeeded)
                {
                    await _signInManager.SignInAsync(user, isPersistent: false);
                    return RedirectToAction(nameof(Index), new { Message = ManageMessageId.RemovePhoneSuccess });
                }
            }
            return RedirectToAction(nameof(Index), new { Message = ManageMessageId.Error });
        }

        //
        // GET: /Manage/ChangePassword
        [HttpGet]
        public IActionResult ChangePassword()
        {
            return View();
        }

        //
        // POST: /Manage/ChangePassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                var result = await _userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
                if (result.Succeeded)
                {
                    await _signInManager.SignInAsync(user, isPersistent: false);
                    _logger.LogInformation(3, "User changed their password successfully.");
                    return RedirectToAction(nameof(Index), new { Message = ManageMessageId.ChangePasswordSuccess });
                }
                AddErrors(result);
                return View(model);
            }
            return RedirectToAction(nameof(Index), new { Message = ManageMessageId.Error });
        }

        //
        // GET: /Manage/SetPassword
        [HttpGet]
        public IActionResult SetPassword()
        {
            return View();
        }

        //
        // POST: /Manage/SetPassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SetPassword(SetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                var result = await _userManager.AddPasswordAsync(user, model.NewPassword);
                if (result.Succeeded)
                {
                    await _signInManager.SignInAsync(user, isPersistent: false);
                    return RedirectToAction(nameof(Index), new { Message = ManageMessageId.SetPasswordSuccess });
                }
                AddErrors(result);
                return View(model);
            }
            return RedirectToAction(nameof(Index), new { Message = ManageMessageId.Error });
        }

        //GET: /Manage/ManageLogins
        [HttpGet]
        public async Task<IActionResult> ManageLogins(ManageMessageId? message = null)
        {
            ViewData["StatusMessage"] =
                message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : message == ManageMessageId.AddLoginSuccess ? "The external login was added."
                : message == ManageMessageId.Error ? "An error has occurred."
                : "";
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return View("Error");
            }
            var userLogins = await _userManager.GetLoginsAsync(user);
            var otherLogins = _signInManager.GetExternalAuthenticationSchemes().Where(auth => userLogins.All(ul => auth.AuthenticationScheme != ul.LoginProvider)).ToList();
            ViewData["ShowRemoveButton"] = user.PasswordHash != null || userLogins.Count > 1;
            return View(new ManageLoginsViewModel
            {
                CurrentLogins = userLogins,
                OtherLogins = otherLogins
            });
        }

        //
        // POST: /Manage/LinkLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult LinkLogin(string provider)
        {
            // Request a redirect to the external login provider to link a login for the current user
            var redirectUrl = Url.Action("LinkLoginCallback", "Manage");
            var properties = _signInManager.ConfigureExternalAuthenticationProperties(provider, redirectUrl, _userManager.GetUserId(User));
            return Challenge(properties, provider);
        }

        //
        // GET: /Manage/LinkLoginCallback
        [HttpGet]
        public async Task<ActionResult> LinkLoginCallback()
        {
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return View("Error");
            }
            var info = await _signInManager.GetExternalLoginInfoAsync(await _userManager.GetUserIdAsync(user));
            if (info == null)
            {
                return RedirectToAction(nameof(ManageLogins), new { Message = ManageMessageId.Error });
            }
            var result = await _userManager.AddLoginAsync(user, info);
            var message = result.Succeeded ? ManageMessageId.AddLoginSuccess : ManageMessageId.Error;
            return RedirectToAction(nameof(ManageLogins), new { Message = message });
        }

        [Authorize]
        [HttpGet]
        public ActionResult Reports(int? page, bool hyperLink = false, string filter = null)
        {
            if (page == null || page == 0)
            {
                page = 1;
            }
            ViewData["Page"] = page;
            if (hyperLink)
            {
                ViewData["hyperLink"] = "true";
            }
            // utility LINQ statements
            var rentals = (from d in _context.Rental select d).ToList();
            var currentRentals = (from d in _context.Rental where DateTime.Now.Subtract(d.DateCreated).TotalDays > 1 select d).ToList();
            var users = (from d in _context.Users select d).ToList();
            var allskis = (from d in _context.Rental where d.SkiBrand != null select d).ToList();
            var TopSkiRental = (from g in _context.Rental where g.SkiBrand != null orderby allskis.Where(f => f.SkiBrand == g.SkiBrand).Count() select g).LastOrDefault().SkiBrand;
            var allboards = (from d in _context.Rental where d.BoardBrand != null select d).ToList();
            var TopSnowboardRental = (from g in _context.Rental where g.BoardBrand != null orderby allboards.Where(f => f.BoardBrand == g.BoardBrand).Count() select g).LastOrDefault().BoardBrand;
            var allbindings = (from d in _context.Rental where d.BindingBrand != null select d).ToList();
            var TopBindingRental = (from g in _context.Rental where g.BindingBrand != null orderby allbindings.Where(f => f.BindingBrand == g.BindingBrand).Count() select g).LastOrDefault().BindingBrand;
            var allboots = (from d in _context.Rental where d.BootBrand != null select d).ToList();
            var TopBootRental = (from g in _context.Rental where g.BootBrand != null orderby allboots.Where(f => f.BootBrand == g.BootBrand).Count() select g).LastOrDefault().BootBrand;
            var allpoles = (from d in _context.Rental where d.PoleBrand != null select d).ToList();
            var TopPoleRental = (from g in _context.Rental where g.PoleBrand != null orderby allpoles.Where(f => f.PoleBrand == g.PoleBrand).Count() select g).LastOrDefault().PoleBrand;

            //To-Be Returned List Assembly
            var ToBeReturnedList = new List<RentalReportViewModel>();
            foreach(var rental in rentals.Where(m=>m.Returned == false))
            {
                DateTime? DueDate = null;
                if (rental.DueDate.HasValue) { DueDate = rental.DueDate.Value; }
                var _return = new RentalReportViewModel()
                {
                    ID = rental.ID,
                    FirstName = rental.FirstName,
                    LastName = rental.LastName,
                    PhoneNumber = rental.PhoneNumber,
                    DateCreated = rental.DateCreated,
                    DailyRental = rental.DailyRental,
                    DueDate = DueDate
                };
                ToBeReturnedList.Add(_return);
            }

            //filtering logic
            if (filter == "Seasonal")
            {
                ToBeReturnedList = ToBeReturnedList.Where(m => m.DailyRental == false).ToList();
            }
            if (filter == "SeasonalRecent")
            {
                ToBeReturnedList = ToBeReturnedList.Where(m => m.DailyRental == false).OrderByDescending(n=>n.DateCreated).ToList();
            }
            if (filter == "Daily")
            {
                ToBeReturnedList = ToBeReturnedList.Where(m => m.DailyRental == true).ToList();
            }
            if (filter == "DailyRecent")
            {
                ToBeReturnedList = ToBeReturnedList.Where(m => m.DueDate != null && m.DueDate.ToString() != "1/1/0001 12:00:00 AM" && DateTime.Now.Subtract(m.DueDate.Value).TotalDays > 1).ToList();
            }
            if (filter == "Recent")
            {
                ToBeReturnedList = ToBeReturnedList.OrderByDescending(n => n.DateCreated).ToList();
            }
            //

            // TODO: Add error handling for daily rentals here
            List<RentalReportViewModel> ToBeReturned = new List<RentalReportViewModel>();
            ViewData["LastPage"] = null;
            //if (((page.Value * 30) + 30) > ToBeReturnedList.Count)
            //{
            //    ToBeReturned = ToBeReturnedList.GetRange((page.Value * 30), (ToBeReturnedList.Count - (page.Value * 30)));
            //    ViewData["LastPage"] = "true";
            //}
            //else
            //{
            //    ToBeReturned = ToBeReturnedList.GetRange((page.Value*30), 30);
            //}

            var report = new ReportViewModel()
            {
                RentalCount = rentals.Count,
                DailyCount = rentals.Where(m => m.DailyRental == true).Count(),
                SeasonalCount = rentals.Where(m => m.DailyRental == false).Count(),
                ReturnedCount = rentals.Where(m => m.Returned == true).Count(),
                UnreturnedCount = rentals.Where(m => m.Returned == false).Count(),
                DailyReturned = rentals.Where(m => m.Returned == true && m.DailyRental == true).Count(),
                DailyUnreturned = rentals.Where(m => m.Returned == false && m.DailyRental == true).Count(),
                CurrentDailyReturned = currentRentals.Where(m => m.Returned == true && m.DailyRental == true).Count(),
                CurrentDailyUnreturned = currentRentals.Where(m => m.Returned == false && m.DailyRental == true).Count(),
                SkiRentals = rentals.Where(m => m.SkiBrand != null).Count(),
                TopSkiRental = TopSkiRental,
                TopSkiRentalCount = rentals.Where(m => m.SkiBrand == TopSkiRental).Count(),
                TopSnowboardRental = TopSnowboardRental,
                TopSnowboardRentalCount = rentals.Where(m => m.BoardBrand == TopSnowboardRental).Count(),
                SnowboardRentals = rentals.Where(m => m.BoardBrand != null).Count(),
                BootRentals = rentals.Where(m => m.BootBrand != null).Count(),
                TopBootRental = TopBootRental,
                TopBootRentalCount = rentals.Where(m => m.BootBrand == TopBootRental).Count(),
                BindingRentals = rentals.Where(m => m.BindingBrand != null).Count(),
                TopBindingRental = TopBindingRental,
                TopBindingRentalCount = rentals.Where(m => m.BindingBrand == TopBindingRental).Count(),
                PoleRentals = rentals.Where(m => m.PoleBrand != null).Count(),
                TopPoleRental = TopPoleRental,
                TopPoleRentalCount = rentals.Where(m => m.PoleBrand == TopPoleRental).Count(),
                UserCount = users.Count,
                PartiallyReturnedCount = rentals.Where(m => m.PartialReturn == true).Count(),
                ToBeReturned = ToBeReturnedList
            };
            return View(report);
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> ToggleCapitalize()
        {
            try
            {
                var user = await GetCurrentUserAsync();
                if (user.Capitalize == true) { user.Capitalize = false; } else { user.Capitalize = true; }
                await _context.SaveChangesAsync();
                CookieOptions cookieOptions = new CookieOptions();
                Response.Cookies.Append("Capitalize", user.Capitalize.ToString(), cookieOptions);
                Response.Cookies.Append("settingChanged", "Capitalize", cookieOptions);
                return RedirectToAction("Index");
            }
            catch
            {
                CookieOptions cookieOptions = new CookieOptions();
                Response.Cookies.Append("settingChanged", "failed", cookieOptions);
                return RedirectToAction("Index");
            }
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> ToggleTextSuggestions()
        {
            try
            {
                var user = await GetCurrentUserAsync();
                if (user.TextSuggestions == true) { user.TextSuggestions = false; } else { user.TextSuggestions = true; }
                await _context.SaveChangesAsync();
                CookieOptions cookieOptions = new CookieOptions();
                Response.Cookies.Append("TextSuggestions", user.TextSuggestions.ToString(), cookieOptions);
                Response.Cookies.Append("settingChanged", "Suggestions", cookieOptions);
                return RedirectToAction("Index");
            }
            catch
            {
                CookieOptions cookieOptions = new CookieOptions();
                Response.Cookies.Append("settingChanged", "failed", cookieOptions);
                return RedirectToAction("Index");
            }
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> ToggleLargeFont()
        {
            try
            {
                var user = await GetCurrentUserAsync();
                if (user.LargeFont == true) { user.LargeFont = false; } else { user.LargeFont = true; }
                await _context.SaveChangesAsync();
                CookieOptions cookieOptions = new CookieOptions();
                Response.Cookies.Append("LargeFont", user.LargeFont.ToString(), cookieOptions);
                Response.Cookies.Append("settingChanged", "Font", cookieOptions);
                return RedirectToAction("Index");
            }
            catch
            {
                CookieOptions cookieOptions = new CookieOptions();
                Response.Cookies.Append("settingChanged", "failed", cookieOptions);
                return RedirectToAction("Index");
            }
        }

        [HttpGet]
        public async Task<IActionResult> ShipLogs()
        {
            #region build out log attatchment
            string LogPath;
            if (System.IO.Directory.Exists(@"C:\Users\user1\Desktop\SkiShop\Logs"))
            {
                LogPath = @"C:\Users\user1\Desktop\SkiShop\Logs";
            }
            else
            {
                LogPath = @"D:\home\LogFiles\nLog";
            }
            var files = System.IO.Directory.GetFiles(LogPath);
            LogPath = System.IO.File.ReadAllLines(files.Where(x=>x.Contains("nlog")).Last()).Aggregate(new StringBuilder(),
                  (sb, a) => sb.AppendLine(String.Join(",", a)),
                  sb => sb.ToString());
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(LogPath);
            LogPath = System.Convert.ToBase64String(plainTextBytes);
            #endregion

            int InfoCount = Regex.Matches(LogPath, "INFO").Count;
            int ErrorCount = Regex.Matches(LogPath, "ERROR").Count;

            var apiKey = "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";
            var client = new SendGridClient(apiKey);
            var from = new EmailAddress("info@serobo.com", "Rental Logistics Webmaster");
            var subject = $"RL_Logs_{DateTime.Now.ToLocalTime().ToString().Replace(" ","")}.txt";
            var to = new EmailAddress("jake.h.marquez@gmail.com", "Jacob Marquez");
            var plainTextContent = $"Logs for {DateTime.Now.ToLocalTime()}{Environment.NewLine}Errors:{ErrorCount}{Environment.NewLine}Info:{InfoCount}";
            //var htmlContent = "<strong>random text</strong>";
            var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent,"");
            if (LogPath != "")
                msg.AddAttachment($"RL Logs {DateTime.Now.ToLocalTime()}", LogPath);
            else
                msg.PlainTextContent += "ERROR PICKING UP LOGS";
            var response = await client.SendEmailAsync(msg);
            return Ok();
        }

        [Authorize]
        [HttpGet]
        public IActionResult Help()
        {
            return View();
        }

        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        public enum ManageMessageId
        {
            AddPhoneSuccess,
            AddLoginSuccess,
            ChangePasswordSuccess,
            ChangeShopPasswordSuccess,
            SetTwoFactorSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            RemovePhoneSuccess,
            ToggleShopPasswordLock,
            Error
        }

        private Task<ApplicationUser> GetCurrentUserAsync()
        {
            return _userManager.GetUserAsync(HttpContext.User);
        }

        #endregion
    }
}
