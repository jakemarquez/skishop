using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Text;
using System.Text.RegularExpressions;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.IO;
using System.Globalization;
using AuthenticatedEncryption;

namespace skiShop.Controllers
{
    public class PublicController : Controller
    {
        public async Task<IActionResult> ShipLogs(string cipher)
        {
            //Public Facing Method
            if (Validate(cipher))
                return await InternalShipLogs();
            //Decryption failed
            else
                return Unauthorized();
        }

        internal bool Validate(string cipher)
        {
            if (String.IsNullOrEmpty(cipher)) { return false; }
            try
            {
                char[] padding = { '=' };
                string key = "9WUGJ521dBoTBJc5Dhy6KTf4E7hXxMy-PWKm363OOo8";
                string incoming = key.Replace('_', '/').Replace('-', '+');
                switch (key.Length % 4)
                {
                    case 2: incoming += "=="; break;
                    case 3: incoming += "="; break;
                }
                byte[] keybytes = Convert.FromBase64String(incoming);

                string cryptkey = "KZV3AGhzaKJGeah8ig67ocmNm2eTPH7IUNJiMNlpZ1Q";
                string cryptincoming = cryptkey.Replace('_', '/').Replace('-', '+');
                switch (cryptkey.Length % 4)
                {
                    case 2: cryptincoming += "=="; break;
                    case 3: cryptincoming += "="; break;
                }
                byte[] cryptkeybytes = Convert.FromBase64String(cryptincoming);
                //byte[] encryptedmessage = Convert.FromBase64String(cipher);
                var plainstring = AuthenticatedEncryption.AuthenticatedEncryption.Decrypt(cipher.Replace('-', '+').Replace('_', '/'), cryptkeybytes, keybytes);
                //var plainstring = System.Convert.ToBase64String(plainbytes).TrimEnd(padding).Replace('+', '-').Replace('/', '_');
                if (plainstring == "PnVq4yElkGhu10DmUJvw25zPqmWbolUaDEx5ignO9fI") { return true; }
                return false;
            }
            catch 
            {
                return false;
            }
        }

        internal async Task<IActionResult> InternalShipLogs()
        {
            #region setup directory
            //re-used string, local copy
            string LogPath, _logPath;
            //local copy
            string unencoded = "";
            int InfoCount = 0;
            int ErrorCount = 0;
            string optional = "";
            if (System.IO.Directory.Exists(@"C:\Users\user1\Desktop\SkiShop\Logs"))
            {
                LogPath = @"C:\Users\user1\Desktop\SkiShop\Logs";
                _logPath = @"C:\Users\user1\Desktop\SkiShop\Logs";
            }
            else
            {
                LogPath = @"D:\home\LogFiles\nLog";
                _logPath = @"D:\home\LogFiles\nLog";
            }
            #endregion

            #region build out log attatchment
            var LogToShip = Directory.GetFiles(_logPath).Where(x => x.Contains(DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd").Replace('.', '-'))).FirstOrDefault();
            if (System.IO.File.Exists(LogToShip)) {  
                var files = System.IO.Directory.GetFiles(LogPath);
                LogPath = System.IO.File.ReadAllLines(files.Where(x => x.Contains("nlog")).Last()).Aggregate(new StringBuilder(),
                      (sb, a) => sb.AppendLine(String.Join(",", a)),
                      sb => sb.ToString());
                unencoded = LogPath;
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(LogPath);
                LogPath = System.Convert.ToBase64String(plainTextBytes);
                InfoCount = Regex.Matches(unencoded, "INFO").Count;
                ErrorCount = Regex.Matches(unencoded, "ERROR").Count;
            }
            else
            {
                LogPath = "";
            }
            #endregion

            #region Cleanup Directory
                if (Directory.GetFiles(_logPath).Where(x => x.Contains("nlog")).Count() > 30)
                {
                    string oldest = Directory.GetFiles(_logPath).Where(x => x.Contains("nlog")).First();
                    optional = $"Warning: Exceeded LOG limit, deleting file {oldest}";
                    System.IO.File.Delete(oldest);
                }
            #endregion

            #region Email Construction
            var apiKey = "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";
            var client = new SendGridClient(apiKey);
            var from = new EmailAddress("info@serobo.com", "Rental Logistics Webmaster");
            var subject = $"RL_Logs_{DateTime.Now.ToLocalTime().ToString().Replace(" ", "")}";
            var to = new List<EmailAddress>(){ 
                //Sending List
                new EmailAddress("jake.h.marquez@gmail.com", "Jacob Marquez"),
                new EmailAddress("devin@serobo.com","Devin Carpenter")
            };
            var plainTextContent = "";
            var htmlContent = $"<h1>LogsFile: {(LogToShip == null ? "None" : Path.GetFileName(LogToShip))}</h1><p><strong>Errors:</strong>{ErrorCount}</p><p><strong>Info:</strong>{InfoCount}</p><p>{(optional == "" ? "" : optional)}</p>";
            var msg = MailHelper.CreateSingleEmailToMultipleRecipients(from, to, subject, plainTextContent, htmlContent);
            if (LogPath != "")
                msg.AddAttachment($"RL Logs {LogToShip.ToString()}", LogPath);
            else
                msg.PlainTextContent += @"Could not find any logs... If this is re-occurring, RDP into VM's D:\home\LogFiles\nLog";
            #endregion
            try
            {
                var response = await client.SendEmailAsync(msg);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest($"Could not complete request. Exception: {e.Message}");
            }
        }
    }
    
}