using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using skiShop.Models;
using Microsoft.AspNetCore.Authorization;
using System.Text;
using skiShop.Services;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace skiShop.Controllers
{
    public class RentalsController : Controller
    {
        private ILogger _logger;
        private readonly skiShopContext _context;
        private RentalService _rentalService;

        public RentalsController(ILogger<RentalsController> logger, skiShopContext context, RentalService rentalService)
        {
            _logger = logger;
            _context = context;
            _rentalService = rentalService;
            link = new List<Rental>();
        }

        // GET: Rentals
        [Authorize]
        public async Task<IActionResult> Index(int Filter, string SearchString, int? page, string currentFilter, int TypeFilter)
        {
            //ViewData["CurrentSort"] = sortOrder;
            if (SearchString != null)
            {
                page = 1;
                CookieOptions cookieOptions = new CookieOptions();
                Response.Cookies.Append("Search", $"{Filter}_{SearchString}_{page}_{SearchString}_{TypeFilter}");
            }
            else
            {
                SearchString = currentFilter;
            }
            ViewData["CurrentFilter"] = SearchString;


            var Rentals = from m in _context.Rental select m;

            if (!string.IsNullOrEmpty(SearchString))
            {
                try
                {
                    if (Filter == 0)
                    {
                        if (!SearchString.Contains(' ')) { Filter = 1; }
                        #region Full Name Logic
                        else
                        {
                            var name = SearchString.Split(' ');
                            var fName = Rentals.Where(r => r.FirstName.Contains(name[0]));
                            var lName = Rentals.Where(r => r.LastName.Contains(name[1]));
                            Rentals = fName.Intersect(lName);
                            var exampl1 = lName.Except(fName);
                            var exampl2 = fName.Except(lName);
                            Rentals = Rentals.Union(exampl1).Union(exampl2);
                            if (TypeFilter == 1)
                            {
                                Rentals = Rentals.Where(m => m.DailyRental == false);
                            }
                            if (TypeFilter == 2)
                            {
                                Rentals = Rentals.Where(m => m.DailyRental == true);
                            }
                        }
                        #endregion
                    }
                    if (Filter == 1)
                    {
                        #region First Name Logic
                        var fNameMatches = Rentals.Where(r => r.FirstName.Contains(SearchString));
                        var lNameMatches = Rentals.Where(r => r.LastName.Contains(SearchString));
                        if (TypeFilter == 0)
                        {
                            Rentals = fNameMatches.Union(lNameMatches);
                        }
                        if (TypeFilter == 1)
                        {
                            Rentals = fNameMatches.Union(lNameMatches).Where(r => r.DailyRental == false);
                        }
                        if (TypeFilter == 2)
                        {
                            Rentals = fNameMatches.Union(lNameMatches).Where(r => r.DailyRental == true);
                        }
                        #endregion
                    }
                    if (Filter == 2)
                    {
                        #region Last Name Logic
                        var fNameMatches = Rentals.Where(r => r.FirstName.Contains(SearchString));
                        var lNameMatches = Rentals.Where(r => r.LastName.Contains(SearchString));
                        if (TypeFilter == 0)
                        {
                            Rentals = lNameMatches.Union(fNameMatches);
                        }
                        if (TypeFilter == 1)
                        {
                            Rentals = lNameMatches.Union(fNameMatches).Where(r => r.DailyRental == false);
                        }
                        if (TypeFilter == 2)
                        {
                            Rentals = lNameMatches.Union(fNameMatches).Where(r => r.DailyRental == true);
                        }
                        #endregion
                    }
                    if (Filter == 3)
                    {
                        #region Email Logic
                        if (TypeFilter == 0)
                        {
                            Rentals = Rentals.Where(r => r.EmailAddress.Contains(SearchString));
                        }
                        if (TypeFilter == 1)
                        {
                            Rentals = Rentals.Where(r => r.EmailAddress.Contains(SearchString)).Where(r => r.DailyRental == false);
                        }
                        if (TypeFilter == 2)
                        {
                            Rentals = Rentals.Where(r => r.EmailAddress.Contains(SearchString)).Where(r => r.DailyRental == true);
                        }
                        #endregion
                    }
                    if (Filter == 4)
                    {
                        #region Phone Number Logic
                        var PhoneNumber1 = Rentals.Where(r => r.PhoneNumber.ToString().Contains(SearchString));
                        var PhoneNumber2 = Rentals.Where(r => r.PhoneNumber2.ToString().Contains(SearchString));
                        foreach (var num in PhoneNumber2)
                        {
                            PhoneNumber1.Append(num);
                        }
                        if (TypeFilter == 0)
                        {
                            Rentals = PhoneNumber1;
                        }
                        if (TypeFilter == 1)
                        {
                            Rentals = PhoneNumber1.Where(r => r.DailyRental == false);
                        }
                        if (TypeFilter == 2)
                        {
                            Rentals = PhoneNumber1.Where(r => r.DailyRental == true);
                        }
                        #endregion
                    }
                    if (Filter == 5)
                    {
                        #region ?????
                        var name = SearchString.Split(' ');
                        var fName = Rentals.Where(r => r.FirstName.Contains(name[0]));
                        var lName = Rentals.Where(r => r.LastName.Contains(name[1]));
                        if (SearchString.Contains(' '))
                        {
                            Rentals = fName.Intersect(lName).Where(r => r.DailyRental == true);
                        }
                        else
                        {
                            Rentals = Rentals.Where(x => x.FirstName.Contains(SearchString) || x.LastName.Contains(SearchString)).Where(r => r.DailyRental == true);
                        }
                        #endregion
                    }
                }
                catch (Exception e) { _logger.LogError($"Error Within Search Algorithm - Exception: {e.Message}"); }
            }
            else
            {
                try
                {
                    if (TypeFilter == 1)
                    {
                        Rentals = Rentals.Where(r => r.DailyRental == false);
                    }
                    if (TypeFilter == 2)
                    {
                        Rentals = Rentals.Where(r => r.DailyRental == true);
                    }
                }
                catch (Exception e) { _logger.LogError($"Error Within Default Search Algorithm - Exception: {e.Message}"); }
            }
            int pageSize = 15;
            ViewData["TotalPages"] = (Rentals.Count() / 15) - (Rentals.Count() % 15);
            //var RentalVM = from d in Rentals select new RentalViewModel4 {
            //                ID = d.ID,
            //                DailyRental = d.DailyRental,
            //                DateCreated = d.DateCreated,
            //                DueDate = d.DueDate,
            //                FirstName = d.FirstName,
            //                LastName = d.LastName,
            //                Address = d.Address,
            //                City = d.City,
            //                State = d.State,
            //                EmailAddress = d.EmailAddress,
            //                PhoneNumber = d.PhoneNumber,
            //                PhoneNumber2 = d.PhoneNumber2,
            //                PartialReturn = d.PartialReturn,
            //                Returned = d.Returned
            //};

            //var RentalVMQuery = RentalVM.AsQueryable();
            try
            {
                return View(await PaginatedList<Rental>.CreateAsync(Rentals.AsNoTracking(),page ?? 1, pageSize));
            }
            catch (Exception e)
            {
                _logger.LogError($"Error creating paginated list in Index Method - Exception: {e.Message}");
                var blankList = new PaginatedList<Rental>(new List<Rental>(),0,0,15);
                return View(blankList);
            }
            //return View(await PaginatedList<RentalViewModel4>.CreateAsync(RentalVM, page ?? 1, pageSize));
        }

        List<int> idset = new List<int>();

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> IndexWithSet(int id, int nextId = 0)
        {
            try
            {
                var selected = from n in _context.Rental where n.ID == id select n;
                if (selected.First().IdPrevious != null && selected.First().IdNext != null)
                {
                    if (nextId == 0) { IndexWithSet(id, int.Parse(selected.First().IdPrevious)).Wait(); }
                    if (nextId != 0)
                    {
                        var _selected = from z in _context.Rental where z.ID == nextId select z;
                        if (_selected.First().IdPrevious == null)
                        {
                            int _id = _selected.First().ID;
                            await IndexWithSet(_id, 0);
                        }
                        else
                        {
                            IndexWithSet(id, int.Parse(_selected.First().IdPrevious)).Wait();
                        }
                    }
                }
                if (selected.First().IdNext == null)
                {
                    if (!idset.Contains(id))
                    {
                        idset.Add(id);
                        IndexWithSet(id, int.Parse(selected.First().IdPrevious)).Wait();
                    }
                    if (nextId != 0)
                    {
                        if (!idset.Contains(nextId)) { idset.Add(nextId); }
                        var _selected = from g in _context.Rental where g.ID == nextId select g;
                        if (_selected.First().IdPrevious != null)
                        {
                            IndexWithSet(id, int.Parse(_selected.First().IdPrevious)).Wait();
                        }
                    }
                    //last rental in set
                }
                if (selected.First().IdPrevious == null)
                {
                    if (!idset.Contains(id))
                    {
                        idset.Add(id);
                        IndexWithSet(id, int.Parse(selected.First().IdNext)).Wait();
                    }
                    if (nextId != 0)
                    {
                        if (!idset.Contains(nextId)) { idset.Add(nextId); }
                        var _selected = from g in _context.Rental where g.ID == nextId select g;
                        if (_selected.First().IdNext != null)
                        {
                            IndexWithSet(id, int.Parse(_selected.First().IdNext)).Wait();
                        }
                    }
                    //first rental in set
                }
            }
            catch(Exception e) {
                _logger.LogError($"IndexWithSet method logic failed. Exception: {e.Message}");
                var blankList = new PaginatedList<Rental>(new List<Rental>(), 0, 0, 15);
                return View(blankList);
            }
            try
            {
                var Rentals = from m in _context.Rental where idset.Contains(m.ID) == true select m;
                int pageSize = 15;
                return View("Index", await PaginatedList<Rental>.CreateAsync(Rentals.AsNoTracking(), 1, pageSize));
            }
            catch(Exception e)
            {
                _logger.LogError($"Error creating Paginated List in IndexWithSet method. Exception: {e.Message}");
                var blankList = new PaginatedList<Rental>(new List<Rental>(), 0, 0, 15);
                return View(blankList);
            }
        }

        [Authorize]
        [HttpGet]
        public IActionResult RedirectToIndex()
        {
            string[] PastSearch = (Request.Cookies["Search"]).Split('_');
            if (PastSearch.Length > 1)
            {
                int Page;
                int.TryParse(PastSearch[2], out Page);
                return RedirectToAction("Index", new { Filter = PastSearch[0], SearchString = PastSearch[1], page = Page, currentFilter = PastSearch[3], TypeFilter = PastSearch[4] });
            }
            return RedirectToAction("Index");
        }

        // GET: Rentals/Details/5
        [Authorize]
        public ActionResult Details(int? id, string returnUrl = null)
        {
            try
            {
                if (id == null)
                {
                    throw new Exception("asdfasdf");
                }

                var rental = (from d in _context.Rental where d.ID == id.Value select d).Include(m => m.Records).SingleOrDefault();
                if (rental == null)
                {
                    return NotFound();
                }

                return View(rental);
            }
            catch(Exception e)
            {
                _logger.LogError($"Error with Details method GET request. Id: {id}. Exception: {e.Message}");
                return RedirectToAction("Index", new { Filter = 0, TypeFilter = 0 });
            }
        }

        [Authorize]
        public async Task<ActionResult> DetailsWithSet(int id, int nextId = 0)
        {
            //TODO track the origin rental that was clicked and hyperlink to in the view
            try
            {
                var selected = from n in _context.Rental where n.ID == id select n;
                if (selected == null) { return NotFound(); }
                if (selected.First().IdPrevious != null && selected.First().IdNext != null)
                {
                    if (nextId == 0) { DetailsWithSet(id, int.Parse(selected.First().IdPrevious)).Wait(); }
                    if (nextId != 0)
                    {
                        var _selected = from z in _context.Rental where z.ID == nextId select z;
                        if (_selected.First().IdPrevious == null)
                        {
                            int _id = _selected.First().ID;
                            await DetailsWithSet(_id, 0);
                        }
                        else
                        {
                            DetailsWithSet(id, int.Parse(_selected.First().IdPrevious)).Wait();
                        }
                    }
                }
                if (selected.First().IdNext == null)
                {
                    if (!idset.Contains(id))
                    {
                        idset.Add(id);
                        DetailsWithSet(id, int.Parse(selected.First().IdPrevious)).Wait();
                    }
                    if (nextId != 0)
                    {
                        if (!idset.Contains(nextId)) { idset.Add(nextId); }
                        var _selected = from g in _context.Rental where g.ID == nextId select g;
                        if (_selected.First().IdPrevious != null)
                        {
                            DetailsWithSet(id, int.Parse(_selected.First().IdPrevious)).Wait();
                        }
                    }
                    //last rental in set
                }
                if (selected.First().IdPrevious == null)
                {
                    if (!idset.Contains(id))
                    {
                        idset.Add(id);
                        DetailsWithSet(id, int.Parse(selected.First().IdNext)).Wait();
                    }
                    if (nextId != 0)
                    {
                        if (!idset.Contains(nextId)) { idset.Add(nextId); }
                        var _selected = from g in _context.Rental where g.ID == nextId select g;
                        if (_selected.First().IdNext != null)
                        {
                            DetailsWithSet(id, int.Parse(_selected.First().IdNext)).Wait();
                        }
                    }
                    //first rental in set
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"Error in DetailsWithSet method logic. Exception: {e.Message}");
                return View(new List<Rental>()); 
            }
            var Rentals = from m in _context.Rental where idset.Contains(m.ID) == true select m;
            return View(Rentals.ToList());
        }

        // GET: Rentals/Create
        [Authorize]
        public IActionResult Create()
        {
            try
            {
                ViewData["FormSecurity"] = _context.AdminOptions.Where(m => m.Name == "CreateForm").FirstOrDefault().Value;
                return View();
            }
            catch (Exception e)
            {
                _logger.LogError($"Error returning Create method GET. Exception: {e.Message}");
                return RedirectToAction("Index", new { Filter = 0, TypeFilter = 0 });
            }
        }

        //[Bind("ID,Address,Age,BindingBrand,BindingSize,BoardBrand,BoardSize,BoardStance,City,CustomerNotes,EmailAddress,FirstName,Height,LastName,LocalAccommodations,PhoneNumber,PhoneNumber2,PoleBrand,PoleSize,Sex,SkiBrand,SkiSize,SkierType,StanceAngles,State,Weight,BootSize,BootBrand,IdSet,IdPrevious")] Rental rental
        // POST: Rentals/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Address,Age,BindingBrand,BindingSize,BoardBrand,BoardSize,BoardStance,City,CustomerNotes,EmailAddress,FirstName,Height,LastName,LocalAccommodations,PhoneNumber,PhoneNumber2,PoleBrand,PoleSize,PostalCode,Sex,SkiBrand,SkiSize,SkierType,StanceAngles,State,Weight,BootSize,BootBrand,DailyRental,BootSoleLength,DIN,DueDate,SnowboardInventoryNumber,SkiInventoryNumber,PoleInventoryNumber,SkiBindingInventoryNumber,SnowboardBindingInventoryNumber,BootsInventoryNumber,SnowboardBindingBrand,SnowboardBindingSize,IsMetric")]Rental rental)
        {
            // int phone1;
            // int.TryParse(rental.PhoneNumber, out phone1);
            //rental.DueDate = DateTime.Parse(rental.DueDate.ToString());
            if (ModelState.IsValid)
            {
                try
                {
                    rental.DateCreated = DateTime.Now;
                    _context.Add(rental);
                    await _context.SaveChangesAsync();
                    // user wants to attatch this to the next entry
                    //if ()
                    //{
                    //return RedirectToAction("Create2",rental.ID);
                    //}
                    var OkResponse = new { url = Url.Action("Index", "Rentals"), err = false };
                    return Ok(OkResponse);
                }
                catch (Exception e)
                {
                    _logger.LogError($"Error in Create method logic. Exception: {e.Message}");
                    return RedirectToAction("Index", new { Filter = 0, TypeFilter = 0 });
                }
            }
            var BadResponse = new { url = Url.Action("Index", "Rentals"), err=true };
            return Ok(BadResponse);
        }

        [Authorize]
        public IActionResult Create2(int? id = null)
        {
            if (id != null)
            {
                var rental = (from d in _context.Rental where d.ID == id select d).FirstOrDefault();
                if (rental != null)
                {
                    rental.IdPrevious = id.ToString();
                }
                return View(rental);
            }
            ViewData["FormSecurity"] = _context.AdminOptions.Where(m => m.Name == "CreateForm").FirstOrDefault().Value;
            return View();
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Create2([Bind("ID,Address,Age,BindingBrand,BindingSize,BoardBrand,BoardSize,BoardStance,City,CustomerNotes,EmailAddress,FirstName,Height,LastName,LocalAccommodations,PhoneNumber,PhoneNumber2,PoleBrand,PoleSize,PostalCode,Sex,SkiBrand,SkiSize,SkierType,StanceAngles,State,Weight,BootSize,BootBrand,DailyRental,BootSoleLength,DIN,DueDate,SnowboardInventoryNumber,SkiInventoryNumber,PoleInventoryNumber,SkiBindingInventoryNumber,SnowboardBindingInventoryNumber,BootsInventoryNumber,SnowboardBindingBrand,SnowboardBindingSize,IdPrevious,IdNext,IsMetric")] Rental rental)
        {
            try
            {
                // CREATE 2 IS USED TO CONTINUALLY ATTATCH RENTALS
                var adminOptions = _context.AdminOptions.Where(m => m.Name == "CreateForm").FirstOrDefault();
                int errors = 0;
                #region Questionable Logic
                // Bootleg Validation
                if (String.IsNullOrWhiteSpace(rental.FirstName)) { errors = errors + 1; ModelState.AddModelError("FirstName", "This Field is Required."); }
                if (String.IsNullOrWhiteSpace(rental.LastName)) { errors = errors + 1; ModelState.AddModelError("LastName", "This Field is Required."); }
                if (String.IsNullOrWhiteSpace(rental.EmailAddress)) { errors = errors + 1; ModelState.AddModelError("EmailAddress", "This Field is Required."); }
                if (String.IsNullOrWhiteSpace(rental.Address)) { errors = errors + 1; ModelState.AddModelError("Address", "This Field is Required."); }
                if (String.IsNullOrWhiteSpace(rental.City)) { errors = errors + 1; ModelState.AddModelError("City", "This Field is Required."); }
                if (String.IsNullOrWhiteSpace(rental.State)) { errors = errors + 1; ModelState.AddModelError("State", "This Field is Required."); }
                if (String.IsNullOrWhiteSpace(rental.PostalCode)) { errors = errors + 1; ModelState.AddModelError("PostalCode", "This Field is Required."); }
                if (rental.PhoneNumber == 0) { errors = errors + 1; ModelState.AddModelError("PhoneNumber", "This Field is Required."); }
                if (adminOptions != null)
                {
                    if (adminOptions.Value == "Strict")
                    {
                        if (String.IsNullOrWhiteSpace(rental.Weight.ToString())) { errors = errors + 1; ModelState.AddModelError("Weight", "This Field is Required."); }
                        if (String.IsNullOrWhiteSpace(rental.Age.ToString())) { errors = errors + 1; ModelState.AddModelError("Age", "This Field is Required."); }
                        if (String.IsNullOrWhiteSpace(rental.Height)) { errors = errors + 1; ModelState.AddModelError("Height", "This Field is Required."); }
                    }
                }
                if (errors > 0) {
                    return View("Create",rental);
                }
                if (ViewData["SetCount"] == null) { ViewData["SetCount"] = 2; }
                else { ViewData["SetCount"] = int.Parse(ViewData["SetCount"].ToString()); }
                //if (ModelState.IsValid)
                //{
                // set the current rentals IdPrevious property to the last id
                //rental.IdPrevious = String.Format("{0}", ViewData["Previous"].ToString());
                //rental.PhoneNumber = double.Parse(rental.PhoneNumber.ToString());
                //if (rental.PhoneNumber == null) { return View("Create", rental); }
                //if (rental.PhoneNumber2 == NaN) { rental.PhoneNumber2 = 0; }
                //rental.PhoneNumber = ((double)rental.PhoneNumber == Double.NaN ? 0 : (double)rental.PhoneNumber);
                //rental.PhoneNumber2 = ((double)rental.PhoneNumber2 == Double.NaN ? 0 : (double)rental.PhoneNumber2);
                #endregion
                rental.Height = rental.Height.Replace(@"\","").Replace("\"","");
                rental.DateCreated = DateTime.Now;
                //rental.DueDate = DateTime.Parse(rental.DueDate.ToString());
                rental.ID = 0;
                _context.Add(rental);
                await _context.SaveChangesAsync();
                // we wants to attatch this to the next entry
                rental.IdPrevious = rental.ID.ToString();
                // reset the now assigned id to 0
                rental.ID = 0;
                int SetCount = 1;
                if (Request.Cookies.Keys.Contains("SetCount"))
                {
                    int.TryParse(Request.Cookies.Where(x => x.Key == "SetCount").First().Value.ToString(),out SetCount);
                    CookieOptions cookieOptions = new CookieOptions();
                    Response.Cookies.Append("SetCount", (SetCount + 1).ToString(), cookieOptions);
                }
                else
                {
                    CookieOptions cookieOptions = new CookieOptions();
                    Response.Cookies.Append("SetCount", "2", cookieOptions);
                }
                ViewData["FormSecurity"] = _context.AdminOptions.Where(m => m.Name == "CreateForm").FirstOrDefault().Value;
                return Ok(new { rental = rental, create = "true"});
            }
            catch(Exception e)
            {
                _logger.LogError($"Error in Create2 method logic. Exception: {e.Message}");
                return RedirectToAction("Index", new { Filter = 0, TypeFilter = 0 });
            }
        }

        public List<Rental> link;

        [HttpPost]
        [Authorize]
        public ActionResult Create3([Bind("ID,Address,Age,BindingBrand,BindingSize,BoardBrand,BoardSize,BoardStance,City,CustomerNotes,EmailAddress,FirstName,Height,LastName,LocalAccommodations,PhoneNumber,PhoneNumber2,PoleBrand,PoleSize,PostalCode,Sex,SkiBrand,SkiSize,SkierType,StanceAngles,State,Weight,BootSize,BootBrand,IdNext,IdPrevious,DailyRental,BootSoleLength,InventoryNumber,DIN,DueDate,IsMetric")] Rental rental)
        {
            try
            {
                // CREATE 3 IS USED TO FINALIZE THE RENTAL SET
                var adminOptions = _context.AdminOptions.Where(m => m.Name == "CreateForm").FirstOrDefault();
                int errors = 0;
                if (String.IsNullOrWhiteSpace(rental.FirstName)) { errors = errors + 1; ModelState.AddModelError("FirstName", "This Field is Required."); }
                if (String.IsNullOrWhiteSpace(rental.LastName)) { errors = errors + 1; ModelState.AddModelError("LastName", "This Field is Required."); }
                if (String.IsNullOrWhiteSpace(rental.EmailAddress)) { errors = errors + 1; ModelState.AddModelError("EmailAddress", "This Field is Required."); }
                if (String.IsNullOrWhiteSpace(rental.Address)) { errors = errors + 1; ModelState.AddModelError("Address", "This Field is Required."); }
                if (String.IsNullOrWhiteSpace(rental.City)) { errors = errors + 1; ModelState.AddModelError("City", "This Field is Required."); }
                if (String.IsNullOrWhiteSpace(rental.State)) { errors = errors + 1; ModelState.AddModelError("State", "This Field is Required."); }
                if (String.IsNullOrWhiteSpace(rental.PostalCode)) { errors = errors + 1; ModelState.AddModelError("PostalCode", "This Field is Required."); }
                if (rental.PhoneNumber == 0) { errors = errors + 1; ModelState.AddModelError("PhoneNumber", "This Field is Required."); }
                if (adminOptions != null)
                {
                    if (adminOptions.Value == "Strict")
                    {
                        if (rental.Weight.ToString() == "") { errors = errors + 1; ModelState.AddModelError("Weight", "This Field is Required."); }
                        if (rental.Age.ToString() == "") { errors = errors + 1; ModelState.AddModelError("Age", "This Field is Required."); }
                        if (String.IsNullOrWhiteSpace(rental.Height)) { errors = errors + 1; ModelState.AddModelError("Height", "This Field is Required."); }
                    }
                }
                if (errors > 0)
                {
                    return View("Create2", rental);
                }
                // Idprevious is the first of the set -- Idprevious not getting reset on the dom in create 2
                Rental Pull = SearchSet(int.Parse(rental.IdPrevious));
                if (rental.ID == 0)
                {
                    rental.DateCreated = DateTime.Now;
                    _context.Add(rental);
                    link.Add(rental);
                }
                if (Pull.IdPrevious != null)
                {
                    link.Add(Pull);
                    Create3(Pull);
                }
                else
                {
                    link.Add(Pull);
                }
                _context.SaveChanges();
                StringBuilder output = new StringBuilder();
                var writer = new StringWriter(output);
                XmlSerializer serializer = new XmlSerializer(typeof(List<Rental>));
                serializer.Serialize(writer, link);
                TempData["link"] = output.ToString();
                return RedirectToAction("EnumerateSet");
            }
            catch (Exception e)
            {
                _logger.LogError($"Error in Create3 method logic. Exception: {e.Message}");
                return RedirectToAction("Index", new { Filter = 0, TypeFilter = 0 });
            }
        }

        [HttpGet]
        [Authorize]
        public ActionResult Create4(int id)
        {
            
            TempData["setId"] = id;
            var selected = _context.Rental.Where(r => r.ID == id).FirstOrDefault();
            if (selected != null)
            {
                ViewData["FormSecurity"] = _context.AdminOptions.Where(m => m.Name == "CreateForm").FirstOrDefault().Value;
                selected.IdPrevious = null;
                selected.IdNext = null;
                return View(selected);
            }
            return View();
        }

        int setId;

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Create4([Bind("ID,Address,Age,BindingBrand,BindingSize,BoardBrand,BoardSize,BoardStance,City,CustomerNotes,EmailAddress,FirstName,Height,LastName,LocalAccommodations,PhoneNumber,PhoneNumber2,PoleBrand,PoleSize,PostalCode,Sex,SkiBrand,SkiSize,SkierType,StanceAngles,State,Weight,BootSize,BootBrand,IdNext,IdPrevious,DailyRental,BootSoleLength,InventoryNumber,DIN,DueDate,IsMetric")] Rental rental)
        {
            // CREATE 4 IS USED TO ADD TO AN EXISTING RENTAL SET
            if (setId == 0) {
                rental.ID = 0;
                setId = int.Parse(TempData["setId"].ToString());
                rental.DateCreated = DateTime.Now;
                _context.Add(rental);
            }
            if (ModelState.IsValid)
            {
                try
                {
                    var _first = (from d in _context.Rental where d.ID == setId select d).FirstOrDefault();
                    if (_first.IdNext != null)
                    {
                        // beginning or middle of set
                        setId = int.Parse(_first.IdNext);
                        return await Create4(rental);
                    }
                    else
                    {
                        await _context.SaveChangesAsync();
                        PatchSet(_first, rental);
                        return RedirectToAction("Index");
                        //end of set
                    }
                }
                catch (Exception e)
                {
                    _logger.LogError($"Error in Create4 method logic. Exception: {e.Message}");
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Index");
        }

        // GET: Rentals/Create
        [Authorize]
        public IActionResult CreateAlt()
        {
            ViewData["FormSecurity"] = _context.AdminOptions.Where(m => m.Name == "CreateForm").FirstOrDefault().Value;
            return View();
        }

        [Authorize]
        public ActionResult ConvertToSet(int id)
        {
            var rental = (from d in _context.Rental where d.ID == id select d).FirstOrDefault();
            if (rental != null)
            {
                CookieOptions cookieOptions = new CookieOptions();
                Response.Cookies.Append("ConvertToSet", rental.ID.ToString(), cookieOptions);
                return RedirectToAction("Index");
            }
            else
            {
                _logger.LogError("Error in ConvertToSet method. Id was not found.");
                return RedirectToAction("Index", new { Filter = 0, TypeFilter = 0 });
            }
        }

        [Authorize]
        public ActionResult AddToSet(int id)
        {
            TempData["setId"] = id;
            try
            {
                var LastRental = _rentalService.FindRentalSetEnd(id);
                var selected = _context.Rental.Where(r => r.ID == id).FirstOrDefault();
                if (selected != null)
                {
                    CookieOptions cookieOptions = new CookieOptions();
                    Response.Cookies.Append("ConvertToSet", LastRental.ID.ToString(), cookieOptions);
                    Response.Cookies.Append("RentalLookup", selected.ID.ToString(), cookieOptions);
                    return RedirectToAction("Index");
                }
                return BadRequest();
            }
            catch (Exception e)
            {
                _logger.LogError($"Error in AddToSet Method. Exception: {e.Message}");
                return RedirectToAction("Index", new { Filter = 0, TypeFilter = 0 });
            }
        }

        [Authorize]
        public Rental SearchSet(int id)
        {
            try
            {
                Rental _first = _context.Rental.Where(r => r.ID == id).First();
                if (_first == null)
                { throw new Exception("_first is null."); }
                return _first;
            }
            catch (Exception e)
            {
                _logger.LogError($"Error in SearchSet method. Exception: {e.Message}");
                return new Rental();
            }
        }

        [Authorize]
        public ActionResult EnumerateSet()
        {
            try
            {
                string xml = TempData["link"].ToString();
                object obj;
                using (XmlReader reader = XmlReader.Create(new StringReader(xml)))
                {
                    reader.MoveToContent();
                    obj = new XmlSerializer(typeof(List<Rental>)).Deserialize(reader);
                }
                List<Rental> _link = obj as List<Rental>;
                _link.Reverse();
                for (var x = 0; x < _link.Count; x++)
                {
                    if ((x + 1) != _link.Count)
                    {
                        _link[x].IdNext = _link[(x + 1)].ID.ToString();
                    }
                }
                _context.UpdateRange(_link);
                _context.SaveChanges();
                //link = new List<Rental>();
                return Ok(new { status = "Success" });
            }
            catch (Exception e)
            {
                _logger.LogError($"Error in EnumerateSet method. Exception {e.Message}");
                return Ok(new { status = "Failure" });
            }
        }

        [Authorize]
        public ActionResult PatchSet(Rental oldRental, Rental rental)
        {
            try
            {
                oldRental.IdNext = rental.ID.ToString();
                rental.IdPrevious = oldRental.ID.ToString();
                _context.Update(oldRental);
                _context.Update(rental);
                _context.SaveChanges();
                return Ok();
            }
            catch (Exception e)
            {
                _logger.LogError($"Error in PatchSet method logic. Exception: {e.Message}");
                return BadRequest();
            }
        }

        [Authorize]
        public ActionResult ReturnSet(int id)
        {
            try
            {
                var selected = _rentalService.FindRentalSetBeginning(id);
                List<int> selectedList = _rentalService.GetRentalSetIdList(selected.ID);
                var Rentals = from m in _context.Rental where selectedList.Contains(m.ID) == true select m;
                foreach (var _rental in Rentals)
                {
                    _rental.Returned = true;
                }
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                _logger.LogError($"Error in ReturnSet method recursive logic. Exception: {e.Message}");
                return Ok();
            }
        }

        // GET: Rentals/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            try
            {
                Rental rental = (from d in _context.Rental where d.ID == id select d).Include(r => r.Records).SingleOrDefault();
                if (id == null)
                {
                    return NotFound();
                }
                if (rental == null)
                {
                    return NotFound();
                }
                var rentalVM = new RentalViewModel()
                {
                    Rental = rental,
                    RentalOld = rental,
                    Save = false
                };
                ViewData["FormSecurity"] = _context.AdminOptions.Where(m => m.Name == "CreateForm").FirstOrDefault().Value;
                return View(rentalVM);
            }
            catch(Exception e)
            {
                _logger.LogError($"Error in Edit GET method. Exception: {e.Message}");
                return RedirectToAction("Index", new { Filter = 0, TypeFilter = 0 });
            }
        }

        // POST: Rentals/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, RentalViewModel RentalViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string[] PastSearch = (Request.Cookies["Search"]).Split('_');
                    int Page;
                    int.TryParse(PastSearch[2], out Page);
                    if (RentalViewModel.Save)
                    {
                        var old = RentalViewModel.RentalOld;
                        string weight = old.IsMetric ?   "kg."  :  "lb." ;
                        Record rec = new Record()
                        {
                            FullName = String.Format("{0} {1}", old.FirstName, old.LastName),
                            Address = String.Format("{0} {1} {2}", old.Address, old.City, old.State),
                            Phone = String.Format("Main: {0} / Alt: {1}", old.PhoneNumber, old.PhoneNumber2),
                            EmailAddress = String.Format("{0}", old.EmailAddress),
                            LocalAccommodations = (String.IsNullOrEmpty(old.LocalAccommodations) ? null : String.Format("Local Accommodations: {0}", old.LocalAccommodations)),
                            HeightWeight = $"Height: {old.Height} / Weight: {old.Weight}{weight}",
                            AgeSex = String.Format("Age: {0} / Sex: {1}", old.Age, old.Sex),
                            SkierType = String.IsNullOrEmpty(old.SkierType) ? null : String.Format("Skier Type: {0}", old.SkierType),
                            SkiBrand = String.IsNullOrEmpty(old.SkiBrand) ? null : String.Format("Ski Brand: {0}", old.SkiBrand),
                            SkiSize = String.IsNullOrEmpty(old.SkiSize) ? null : String.Format("Ski Size: {0}", old.SkiSize),
                            SkiInventoryNumber = String.IsNullOrEmpty(old.SkiInventoryNumber) ? null : $"Ski Inventory Number: {old.SkiInventoryNumber}",
                            BoardBrand = String.IsNullOrEmpty(old.BoardBrand) ? null : String.Format("Board Brand: {0}", old.BoardBrand),
                            BoardStance = String.IsNullOrEmpty(old.BoardStance) ? null : String.Format("Board Stance: {0}", old.BoardStance),
                            BoardSize = String.IsNullOrEmpty(old.BoardSize) ? null : String.Format("Board Size: {0}", old.BoardSize),
                            StanceAngles = String.IsNullOrEmpty(old.StanceAngles) ? null : String.Format("Stance Angles: {0}", old.StanceAngles),
                            SnowboardInventoryNumber = String.IsNullOrEmpty(old.SnowboardInventoryNumber) ? null : $"Board Inventory Number: {old.SnowboardInventoryNumber}",
                            PoleSize = String.IsNullOrEmpty(old.PoleSize) ? null : String.Format("Pole Size: {0}", old.PoleSize),
                            PoleBrand = String.IsNullOrEmpty(old.PoleBrand) ? null : String.Format("Pole Brand: {0}", old.PoleBrand),
                            PoleInventoryNumber = String.IsNullOrEmpty(old.PoleInventoryNumber) ? null : $"Pole Inventory Number: {old.PoleInventoryNumber}",
                            BootBrand = String.IsNullOrEmpty(old.BootBrand) ? null : String.Format("Boot Brand: {0}", old.BootBrand),
                            BootSize = String.IsNullOrEmpty(old.BootSize) ? null : String.Format("Boot Size: {0}", old.BootSize),
                            BootSoleLength = String.IsNullOrEmpty(old.BootSoleLength) ? null : String.Format("Boot Sole Length: {0}", old.BootSoleLength),
                            BootInventoryNumber = String.IsNullOrEmpty(old.BootsInventoryNumber) ? null : $"Boot Inventory Number: {old.BootsInventoryNumber}",
                            BindingBrand = String.IsNullOrEmpty(old.BindingBrand) ? null : String.Format("Ski Binding Brand: {0}", old.BindingBrand),
                            BindingSize = String.IsNullOrEmpty(old.BindingSize) ? null : String.Format("Ski Binding Size: {0}", old.BindingSize),
                            SkiBindingInventoryNumber = String.IsNullOrEmpty(old.SkiBindingInventoryNumber) ? null : $"Ski Binding Inventory Number: {old.SkiBindingInventoryNumber}",
                            SnowboardBindingBrand = String.IsNullOrEmpty(old.SnowboardBindingBrand) ? null : String.Format("Board Binding Brand: {0}", old.SnowboardBindingBrand),
                            SnowboardBindingSize = String.IsNullOrEmpty(old.SnowboardBindingSize) ? null : String.Format("Board Binding Size: {0}", old.SnowboardBindingSize),
                            SnowboardBindingInventoryNumber = String.IsNullOrEmpty(old.SnowboardBindingInventoryNumber) ? null : $"Board Binding Inventory Number: {old.SnowboardBindingInventoryNumber}",
                            DIN = String.IsNullOrEmpty(old.DIN) ? null : String.Format("DIN: {0}", old.DIN),
                            CustomerNotes = String.IsNullOrEmpty(old.CustomerNotes) ? null : String.Format("Customer Notes: {0}", old.CustomerNotes),
                            Returned = old.Returned,
                            LogTime = DateTime.Now
                        };

                        //var old = (from d in _context.Rental where d.ID == RentalViewModel.Rental.ID select d).Include(d => d.Records).FirstOrDefault();
                        if (RentalViewModel.Rental.Records != null)
                        {
                            if (RentalViewModel.RentalOld != null)
                            {
                                RentalViewModel.Rental.Records.Add(rec);
                            }
                        }
                        else
                        {
                            return BadRequest();
                        }
                        _context.Update(RentalViewModel.Rental);
                        _context.SaveChanges();
                        //AddRecord(RentalViewModel.Rental.ID);
                        return RedirectToAction("Details", new { ID = id });
                        //return RedirectToAction("Index", new { Filter = PastSearch[0], SearchString = PastSearch[1], page = Page, currentFilter = PastSearch[3], TypeFilter = PastSearch[4] });
                    }
                    _context.Update(RentalViewModel.Rental);
                    _context.SaveChanges();
                    return RedirectToAction("Details", new { ID = id });
                    //return RedirectToAction("Index", new { Filter = PastSearch[0], SearchString = PastSearch[1], page = Page, currentFilter = PastSearch[3], TypeFilter = PastSearch[4] });
                }
                return View(RentalViewModel);
            }
            catch (Exception e)
            {
                _logger.LogError($"Error in Edit POST Method logic. Exception: {e.Message}");
                return RedirectToAction("Index", new { Filter = 0, TypeFilter = 0 });
            }
        }
        
        [Authorize]
        [HttpGet]
        public ActionResult ReturnRental(int id)
        {
            try
            {
                var Rental = (from d in _context.Rental where d.ID == id select d).FirstOrDefault();
                var _rental = new RentalViewModel3()
                {
                    Rental = Rental,
                    message = "",
                    status = 0
                };
                return View(_rental);
            }
            catch (Exception e)
            {
                _logger.LogError($"Error in ReturnRental GET method. Exception: {e.Message}");
                return RedirectToAction("Index", new { Filter = 0, TypeFilter = 0 });
            }
        }

        [Authorize]
        [HttpPost]
        public ActionResult ReturnRental(RentalViewModel3 _rental)
        {
            try
            {
                var rental = (from d in _context.Rental where d.ID == _rental.Rental.ID select d).FirstOrDefault();
                if (rental != null)
                {
                    switch (_rental.status)
                    {
                        case 0:
                            rental.Returned = true;
                            rental.PartialReturn = false;
                            rental.PartialReturnMessage = "";
                            _context.Update(rental);
                            _context.SaveChanges();
                            return RedirectToAction("RedirectToIndex");
                        case 1:
                            rental.PartialReturn = false;
                            rental.Returned = false;
                            rental.PartialReturnMessage = "";
                            _context.Update(rental);
                            _context.SaveChanges();
                            return RedirectToAction("RedirectToIndex");
                        case 2:
                            rental.Returned = false;
                            rental.PartialReturn = true;
                            rental.PartialReturnMessage = _rental.message;
                            _context.Update(rental);
                            _context.SaveChanges();
                            return RedirectToAction("RedirectToIndex");
                    }
                }
                return BadRequest("Index");
            }
            catch (Exception e)
            {
                _logger.LogError($"Error in ReturnRental POST logic. Exception: {e.Message}");
                return BadRequest("Index");
            }
        }

        [Authorize]
        public ActionResult RetrieveRental(int id)
        {
            try
            {
                Rental rental = (from d in _context.Rental where d.ID == id select d).FirstOrDefault();
                return Ok(rental);
            }
            catch(Exception e)
            {
                _logger.LogError($"Error in RetrieveRental GET Method. Exception: {e.Message}");
                return BadRequest();
            }
        }

        // GET: Rentals/Delete/5
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var rental = await _context.Rental.SingleOrDefaultAsync(m => m.ID == id);
                if (rental == null)
                {
                    return NotFound();
                }

                return View(rental);
            }
            catch(Exception e)
            {
                _logger.LogError($"Error in Delete GET Method. Exception {e.Message}");
                return RedirectToAction("Index", new { Filter = 0, TypeFilter = 0 });
            }
        }

        // POST: Rentals/Delete/5
        [HttpPost, ActionName("Delete")]
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var rental = await _context.Rental.Where(m => m.ID == id).Include(m => m.Records).SingleOrDefaultAsync();
                if (rental.IdPrevious != null || rental.IdNext != null)
                {
                    await DeleteFromSet(rental);
                    return RedirectToAction("RedirectToIndex");
                }
                _context.Rental.Remove(rental);
                await _context.SaveChangesAsync();
                return RedirectToAction("RedirectToIndex");
            }
            catch (Exception e)
            {
                _logger.LogError($"Error in DeleteConfirmed POST Method. Exception: {e.Message}");
                return RedirectToAction("Index", new { Filter = 0, TypeFilter = 0 });
            }
        }

        [Authorize]
        public async Task<ActionResult> DeleteFromSet(Rental rental)
        {
            try
            {
                if (rental.IdNext != null && rental.IdPrevious != null)
                {
                    // in the middle of a set
                    var prev = await _context.Rental.Where(m => m.ID == int.Parse(rental.IdPrevious)).SingleOrDefaultAsync();
                    var next = await _context.Rental.Where(m => m.ID == int.Parse(rental.IdNext)).SingleOrDefaultAsync();
                    prev.IdNext = next.ID.ToString();
                    next.IdPrevious = prev.ID.ToString();
                    _context.Remove(rental);
                    await _context.SaveChangesAsync();
                    return Ok();
                }
                if (rental.IdPrevious != null)
                {
                    var prev = await _context.Rental.Where(m => m.ID == int.Parse(rental.IdPrevious)).SingleOrDefaultAsync();
                    if (prev != null)
                    {
                        prev.IdNext = null;
                    }
                    _context.Remove(rental);
                    await _context.SaveChangesAsync();
                    return Ok();
                    // end of the set
                }
                if (rental.IdNext != null)
                {
                    var next = await _context.Rental.Where(m => m.ID == int.Parse(rental.IdNext)).SingleOrDefaultAsync();
                    if (next != null)
                    {
                        next.IdPrevious = null;
                    }
                    _context.Remove(rental);
                    await _context.SaveChangesAsync();
                    return Ok();
                    // start of the set
                }
                return BadRequest();
            }
            catch (Exception e)
            {
                _logger.LogError($"Error in DeleteFromSet Method. Exception {e.Message}");
                return BadRequest();
            }
        }

        List<int> idSet2 = new List<int>();

        [Authorize]
        public ActionResult DeleteSet(int id)
        {
            try
            {
                var selected = _rentalService.FindRentalSetBeginning(id);
                List<int> selectedList = _rentalService.GetRentalSetIdList(selected.ID);
                var Rentals = from m in _context.Rental where selectedList.Contains(m.ID) == true select m;
                _context.RemoveRange(Rentals);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                _logger.LogError($"Error in DeleteSet Method logic. Exception: {e.Message}");
                return BadRequest();
            }
        }

        [Authorize]
        private bool RentalExists(int id)
        {
            try
            {
                return _context.Rental.Any(e => e.ID == id);
            }
            catch(Exception e)
            {
                _logger.LogError($"Error in RentalExists Method. Exception {e.Message}");
                return false;
            }
        }

        [HttpGet, ActionName("UpdateRental")]
        private ActionResult UpdateRental(RentalViewModel RentalViewModel)
        {
            try
            {
                _context.Update(RentalViewModel.Rental);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                _logger.LogError($"Error in UpdateRental GET Method. Exception {e.Message}");
                return RedirectToAction("Index", new { Filter = 0, TypeFilter = 0 });
            }
        }

    }
}
