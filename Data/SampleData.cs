﻿using skiShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;

namespace skiShop.Data
{
    public class SampleData 
    {
        public static void InitializeAsync(IServiceProvider serviceProvider)
        {
            using (var context = new skiShopContext(
                serviceProvider.GetRequiredService<DbContextOptions<skiShopContext>>()))
            {
                // Look for any movies.
                if (context.ShopPassword.Any())
                {
                    return;   // DB has been seeded
                }

                context.ShopPassword.Add(
                     new ShopPassword()
                     {
                         Password = "Password_1",
                     }
                );
                context.SaveChanges();
            }
        }
    }
}
