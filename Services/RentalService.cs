﻿using skiShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace skiShop.Services
{
    public class RentalService
    {
        private readonly skiShopContext _context;
        public Rental FindRentalSetBeginning(int id)
        {
            var selected = (from n in _context.Rental where n.ID == id select n).FirstOrDefault();
            if (selected.IdPrevious != null || (selected.IdPrevious != null && selected.IdNext != null))
            {
                // Not the beginning
                var prev = from n in _context.Rental where n.ID == int.Parse(selected.IdPrevious) select n;
                return FindRentalSetBeginning(prev.FirstOrDefault().ID);
            }
            if (selected.IdPrevious == null)
            {
                // First rental in a set
                return selected;
            }
            else
            {
                return new Rental();
            }
        }

        public Rental FindRentalSetEnd(int id)
        {
            var selected = (from n in _context.Rental where n.ID == id select n).FirstOrDefault();
            if (selected.IdNext != null || (selected.IdPrevious != null && selected.IdNext != null))
            {
                // Not the beginning
                var next = from n in _context.Rental where n.ID == int.Parse(selected.IdNext) select n;
                return FindRentalSetEnd(next.FirstOrDefault().ID);
            }
            if (selected.IdNext == null)
            {
                // First rental in a set
                return selected;
            }
            else
            {
                return new Rental();
            }
        }

        public List<int> GetRentalSetIdList(int id)
        {
            List<int> IdSet = new List<int>();
            RecursiveRentalGathering(id, IdSet);
            return IdSet;
        }

        private List<int> RecursiveRentalGathering(int id, List<int> IdSet)
        {
            if (!IdSet.Contains(id))
            {
                //this is the last one to add
                IdSet.Add(id);
                return RecursiveRentalGathering(id, IdSet);
            }
            else
            {
                var selected = (from n in _context.Rental where n.ID == id select n).FirstOrDefault();
                var next = from n in _context.Rental where n.ID == int.Parse(selected.IdNext) select n;
                if (selected.IdNext == null)
                {
                    return IdSet;
                }
                else
                {
                    return RecursiveRentalGathering(next.FirstOrDefault().ID, IdSet);
                }
            }
        }

        public RentalService(skiShopContext context)
        {
            _context = context;
        }
    }
}
