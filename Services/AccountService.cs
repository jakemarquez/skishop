﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using skiShop.Models;
using Microsoft.EntityFrameworkCore;
using System.Text.RegularExpressions;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace skiShop.Services
{
    public class AccountService : Controller
    {
        private skiShopContext _SkiShopContext { get; set; }
        public AccountService(skiShopContext skiShopContext)
        {
            _SkiShopContext = skiShopContext;
        }
        private string ShopMasterPassword { get; set; }
        public bool setShopPassword(string newPass, string oldPass)
        {
            Regex num = new Regex(@"[0-9]");
            Regex characters = new Regex(@"[!@#$%^&*_.,-]");
            if (String.IsNullOrWhiteSpace(newPass))
            {
                throw new ArgumentException("New Password Required.");
            }
            if (newPass.Length < 6)
            {
                throw new ArgumentException("New Password Too Short.");
            }
            if (newPass.Length > 20)
            {
                throw new ArgumentException("New Password Too Long.");
            }
            if (newPass.Contains(" "))
            {
                throw new ArgumentException("New Password Cannot Contain Spaces.");
            }
            if (!num.IsMatch(newPass))
            {
                throw new ArgumentException("New Password Must Contain A Number.");
            }
            if (!characters.IsMatch(newPass))
            {
                throw new ArgumentException("New Password Must Contain A Symbol.");
            }
            var oldMatch = _SkiShopContext.ShopPassword.First().Password.Equals(oldPass);
            if (!oldMatch) { throw new ArgumentException("Old Password Is Incorrect."); }
            if (_SkiShopContext.ShopPassword.Any())
            {
                foreach (var key in _SkiShopContext.ShopPassword)
                {
                    _SkiShopContext.ShopPassword.Remove(key);
                }
            }
            _SkiShopContext.ShopPassword.Add(new ShopPassword
            {
                Password = newPass
            });
            _SkiShopContext.SaveChanges();
            return true;
        }

        public bool CheckPass(string pass)
        {
            if (pass != _SkiShopContext.ShopPassword.First().Password)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool ShopPasswordUnlocked()
        {
            var _lock = (from d in _SkiShopContext.AdminOptions where d.Name == "ShopPasswordLocked" select d).FirstOrDefault();
            if (_lock == null) { return false; }
            if (_lock.Value == "False") { return false; }
            return true;
        }

        public string LockShopPassword()
        {
            var options = _SkiShopContext.AdminOptions;
            if (options.FirstOrDefault() == null)
            {
                options.Add(new AdminOption()
                {
                    Name = "ShopPasswordLocked",
                    Value = "True"
                });
                _SkiShopContext.SaveChanges();
                return "Locked";
            }
            if(options.First().Value == "True")
            {
                options.First().Value = "False";
                _SkiShopContext.SaveChanges();
                return "Unlocked";
            }
            else
            {
                options.First().Value = "True";
                _SkiShopContext.SaveChanges();
                return "Locked";
            }
        }

        public string ToggleCreateFormStrictMode()
        {
            // Toggles the administrator setting for Create Form 
            var _default = (from d in _SkiShopContext.AdminOptions where d.Name == "CreateForm" select d).FirstOrDefault();
            if (_default == null)
            {
                _SkiShopContext.AdminOptions.Add(new AdminOption()
                {
                    Name = "CreateForm",
                    Value = "Strict"
                });
                _SkiShopContext.SaveChanges();
                return "Strict";
            }
            else
            {
                if (_default.Value == "Strict")
                {
                    _default.Value = "Lenient";
                }
                else
                {
                    _default.Value = "Strict";
                }
            }
            _SkiShopContext.SaveChanges();
            return _default.Value.ToString();
        }
    }
}
