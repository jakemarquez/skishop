﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace skiShop.Migrations
{
    public partial class forty : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "BootSoleLength",
                table: "Rental",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DIN",
                table: "Rental",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "InventoryNumber",
                table: "Rental",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BootSoleLength",
                table: "Rental");

            migrationBuilder.DropColumn(
                name: "DIN",
                table: "Rental");

            migrationBuilder.DropColumn(
                name: "InventoryNumber",
                table: "Rental");
        }
    }
}
