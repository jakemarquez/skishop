﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace skiShop.Migrations
{
    public partial class fiftyone : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "PartialReturn",
                table: "Rental",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "PartialReturnMessage",
                table: "Rental",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PartialReturn",
                table: "Rental");

            migrationBuilder.DropColumn(
                name: "PartialReturnMessage",
                table: "Rental");
        }
    }
}
