﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace skiShop.Migrations
{
    public partial class fifty : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "PhoneNumber2",
                table: "Rental",
                nullable: false);

            migrationBuilder.AlterColumn<double>(
                name: "PhoneNumber",
                table: "Rental",
                nullable: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "PhoneNumber2",
                table: "Rental",
                nullable: false);

            migrationBuilder.AlterColumn<int>(
                name: "PhoneNumber",
                table: "Rental",
                nullable: false);
        }
    }
}
