﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using skiShop.Models;

namespace skiShop.Migrations
{
    [DbContext(typeof(skiShopContext))]
    [Migration("20170429174925_fiftythree")]
    partial class fiftythree
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole", b =>
                {
                    b.Property<string>("Id");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("NormalizedName")
                        .HasAnnotation("MaxLength", 256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .HasName("RoleNameIndex");

                    b.ToTable("AspNetRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("skiShop.Models.AdminOption", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("ID");

                    b.ToTable("AdminOptions");
                });

            modelBuilder.Entity("skiShop.Models.ApplicationUser", b =>
                {
                    b.Property<string>("Id");

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Email")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("NormalizedUserName")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasAnnotation("MaxLength", 256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex");

                    b.ToTable("AspNetUsers");
                });

            modelBuilder.Entity("skiShop.Models.Record", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address");

                    b.Property<string>("AgeSex");

                    b.Property<string>("BindingBrand");

                    b.Property<string>("BindingSize");

                    b.Property<string>("BoardBrand");

                    b.Property<string>("BoardSize");

                    b.Property<string>("BoardStance");

                    b.Property<string>("BootBrand");

                    b.Property<string>("BootSize");

                    b.Property<string>("BootSoleLength");

                    b.Property<string>("CustomerNotes");

                    b.Property<string>("DIN");

                    b.Property<string>("EmailAddress");

                    b.Property<string>("FullName")
                        .IsRequired();

                    b.Property<string>("HeightWeight");

                    b.Property<string>("InventoryNumber");

                    b.Property<string>("LocalAccommodations");

                    b.Property<DateTime>("LogTime");

                    b.Property<string>("Phone")
                        .IsRequired();

                    b.Property<string>("PoleBrand");

                    b.Property<string>("PoleSize");

                    b.Property<bool>("PublicSubmission");

                    b.Property<int?>("RentalID");

                    b.Property<bool>("Returned");

                    b.Property<string>("SkiBrand");

                    b.Property<string>("SkiSize");

                    b.Property<string>("SkierType");

                    b.Property<string>("StanceAngles");

                    b.HasKey("Id");

                    b.HasIndex("RentalID");

                    b.ToTable("Records");
                });

            modelBuilder.Entity("skiShop.Models.Rental", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address");

                    b.Property<int>("Age");

                    b.Property<string>("BindingBrand");

                    b.Property<string>("BindingSize");

                    b.Property<string>("BoardBrand");

                    b.Property<string>("BoardSize");

                    b.Property<string>("BoardStance");

                    b.Property<string>("BootBrand");

                    b.Property<string>("BootSize");

                    b.Property<string>("BootSoleLength");

                    b.Property<string>("City");

                    b.Property<string>("CustomerNotes");

                    b.Property<string>("DIN");

                    b.Property<bool>("DailyRental");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DueDate");

                    b.Property<string>("EmailAddress");

                    b.Property<string>("FirstName");

                    b.Property<string>("Height");

                    b.Property<string>("IdNext");

                    b.Property<string>("IdPrevious");

                    b.Property<string>("InventoryNumber");

                    b.Property<string>("LastName");

                    b.Property<string>("LocalAccommodations");

                    b.Property<bool>("PartialReturn");

                    b.Property<string>("PartialReturnMessage");

                    b.Property<double>("PhoneNumber");

                    b.Property<double>("PhoneNumber2");

                    b.Property<string>("PoleBrand");

                    b.Property<string>("PoleSize");

                    b.Property<string>("PostalCode");

                    b.Property<bool>("PublicSubmission");

                    b.Property<bool>("Returned");

                    b.Property<string>("Sex");

                    b.Property<string>("SkiBrand");

                    b.Property<string>("SkiSize");

                    b.Property<string>("SkierType");

                    b.Property<string>("StanceAngles");

                    b.Property<string>("State");

                    b.Property<int>("Weight");

                    b.HasKey("ID");

                    b.ToTable("Rental");
                });

            modelBuilder.Entity("skiShop.Models.ShopPassword", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Password")
                        .IsRequired();

                    b.HasKey("ID");

                    b.ToTable("ShopPassword");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole")
                        .WithMany("Claims")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("skiShop.Models.ApplicationUser")
                        .WithMany("Claims")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("skiShop.Models.ApplicationUser")
                        .WithMany("Logins")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole")
                        .WithMany("Users")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("skiShop.Models.ApplicationUser")
                        .WithMany("Roles")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("skiShop.Models.Record", b =>
                {
                    b.HasOne("skiShop.Models.Rental")
                        .WithMany("Records")
                        .HasForeignKey("RentalID");
                });
        }
    }
}
