﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace skiShop.Migrations
{
    public partial class fiftyeight : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "BootInventoryNumber",
                table: "Records",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PoleInventoryNumber",
                table: "Records",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SkiBindingInventoryNumber",
                table: "Records",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SkiInventoryNumber",
                table: "Records",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SnowboardBindingBrand",
                table: "Records",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SnowboardBindingInventoryNumber",
                table: "Records",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SnowboardBindingSize",
                table: "Records",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SnowboardInventoryNumber",
                table: "Records",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BootInventoryNumber",
                table: "Records");

            migrationBuilder.DropColumn(
                name: "PoleInventoryNumber",
                table: "Records");

            migrationBuilder.DropColumn(
                name: "SkiBindingInventoryNumber",
                table: "Records");

            migrationBuilder.DropColumn(
                name: "SkiInventoryNumber",
                table: "Records");

            migrationBuilder.DropColumn(
                name: "SnowboardBindingBrand",
                table: "Records");

            migrationBuilder.DropColumn(
                name: "SnowboardBindingInventoryNumber",
                table: "Records");

            migrationBuilder.DropColumn(
                name: "SnowboardBindingSize",
                table: "Records");

            migrationBuilder.DropColumn(
                name: "SnowboardInventoryNumber",
                table: "Records");
        }
    }
}
