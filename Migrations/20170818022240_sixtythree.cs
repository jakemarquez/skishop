﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace skiShop.Migrations
{
    public partial class sixtythree : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ImperialSystem",
                table: "Rental");

            migrationBuilder.AddColumn<bool>(
                name: "IsMetric",
                table: "Rental",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsMetric",
                table: "Rental");

            migrationBuilder.AddColumn<bool>(
                name: "ImperialSystem",
                table: "Rental",
                nullable: false,
                defaultValue: false);
        }
    }
}
