﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace skiShop.Migrations
{
    public partial class fortyone : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "BootBrand",
                table: "Records",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BootSize",
                table: "Records",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BootSoleLength",
                table: "Records",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DIN",
                table: "Records",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "InventoryNumber",
                table: "Records",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "PublicSubmission",
                table: "Records",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BootBrand",
                table: "Records");

            migrationBuilder.DropColumn(
                name: "BootSize",
                table: "Records");

            migrationBuilder.DropColumn(
                name: "BootSoleLength",
                table: "Records");

            migrationBuilder.DropColumn(
                name: "DIN",
                table: "Records");

            migrationBuilder.DropColumn(
                name: "InventoryNumber",
                table: "Records");

            migrationBuilder.DropColumn(
                name: "PublicSubmission",
                table: "Records");
        }
    }
}
