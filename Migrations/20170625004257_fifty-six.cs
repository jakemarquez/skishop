﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace skiShop.Migrations
{
    public partial class fiftysix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "BootsInventoryNumber",
                table: "Rental",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PoleInventoryNumber",
                table: "Rental",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SkiBindingInventoryNumber",
                table: "Rental",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SkiInventoryNumber",
                table: "Rental",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SnowboardBindingBrand",
                table: "Rental",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SnowboardBindingInventoryNumber",
                table: "Rental",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SnowboardBindingSize",
                table: "Rental",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SnowboardInventoryNumber",
                table: "Rental",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BootsInventoryNumber",
                table: "Rental");

            migrationBuilder.DropColumn(
                name: "PoleInventoryNumber",
                table: "Rental");

            migrationBuilder.DropColumn(
                name: "SkiBindingInventoryNumber",
                table: "Rental");

            migrationBuilder.DropColumn(
                name: "SkiInventoryNumber",
                table: "Rental");

            migrationBuilder.DropColumn(
                name: "SnowboardBindingBrand",
                table: "Rental");

            migrationBuilder.DropColumn(
                name: "SnowboardBindingInventoryNumber",
                table: "Rental");

            migrationBuilder.DropColumn(
                name: "SnowboardBindingSize",
                table: "Rental");

            migrationBuilder.DropColumn(
                name: "SnowboardInventoryNumber",
                table: "Rental");
        }
    }
}
