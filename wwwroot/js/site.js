﻿
function filterForm(type) {
    var skiType = document.getElementById("skierTypeDiv");
    var skiBrand = document.getElementById("skiBrandDiv");
    var skiSize = document.getElementById("skiSizeDiv");
    var boardBrand = document.getElementById("boardBrandDiv");
    var boardSize = document.getElementById("boardSizeDiv");
    var boardStance = document.getElementById("boardStanceDiv");
    var stanceAngles = document.getElementById("stanceAnglesDiv");
    var poleSize = document.getElementById("poleSizeDiv");
    var poleBrand = document.getElementById("poleBrandDiv");
    switch (type) {
        case 0:
            skiType.removeAttribute("style");
            skiBrand.removeAttribute("style");
            skiSize.removeAttribute("style");
            boardBrand.removeAttribute("style");
            boardSize.removeAttribute("style");
            boardStance.removeAttribute("style");
            stanceAngles.removeAttribute("style");
            poleSize.removeAttribute("style");
            poleBrand.removeAttribute("style");
            break;
        case 1:
            skiType.removeAttribute("style");
            skiBrand.removeAttribute("style");
            skiSize.removeAttribute("style");
            poleSize.removeAttribute("style");
            poleBrand.removeAttribute("style");
            boardBrand.setAttribute("style", "display:none;");
            boardSize.setAttribute("style", "display:none;");
            boardStance.setAttribute("style", "display:none;");
            stanceAngles.setAttribute("style", "display:none;");
            break;
        case 2:
            boardBrand.removeAttribute("style");
            boardSize.removeAttribute("style");
            boardStance.removeAttribute("style");
            stanceAngles.removeAttribute("style");
            skiType.setAttribute("style", "display:none;");
            skiBrand.setAttribute("style", "display:none;");
            skiSize.setAttribute("style", "display:none;");
            poleSize.setAttribute("style", "display:none;");
            poleBrand.setAttribute("style", "display:none;");
            break;
    }

}

function togglePhoneNumber2() {
    if (document.getElementById("phoneNumber2Div").getAttribute("style") === "display:none;") {
        document.getElementById("phoneNumber2Div").removeAttribute("style");
    }
    else {
        document.getElementById("phoneNumber2Div").setAttribute("style", "display:none;");
    }
}

function toggleAddress2() {
    if (document.getElementById("localAccommodationsDiv").getAttribute("style") === "display:none;") {
        document.getElementById("localAccommodationsDiv").removeAttribute("style");
    }
    else {
        document.getElementById("localAccommodationsDiv").setAttribute("style", "display:none;");
    }
}

var skiBrands = ["Rossignol", "Nordica", "Salomon", "Fischer", "Stoeckli", "Volkl", "Blizzard", "Dynastar", "Madschus", "Burton", "Atomic", "Head", "K2", "Tecnica", "Dynastar", "Lib Tech", "Gnu", "Union", "Liberty", "Marker", "Tyrolia", "Flux", "Burton", "Armada", "Fischer", "Capita", "Never Summer", "Line", "Thirty Two", "Leki", "Komperdell", "Kerma","Swix","Scott","Dalbello","Lange","Excite"];
var snowboardBrands = ["Rossignol", "Nordica", "Salomon", "Fischer", "Stoeckli", "Voelkl", "Blizzard", "Dynastar", "Madschus", "Burton", "Atomic", "Head", "K2", "Technica", "Dynastar", "Lib Tech", "Gnu", "Union", "Liberty", "Marker", "Tyrolia", "Flux", "Burton", "Armada", "Fischer", "Capita", "Never Summer", "Line", "Thirty Two", "Leki", "Komperdell", "Kerma"];
var poleBrands = ["Rossignol", "Nordica", "Salomon", "Fischer", "Stoeckli", "Voelkl", "Blizzard", "Dynastar", "Madschus", "Burton", "Atomic", "Head", "K2", "Technica", "Dynastar", "Lib Tech", "Gnu", "Union", "Liberty", "Marker", "Tyrolia", "Flux", "Burton", "Armada", "Fischer", "Capita", "Never Summer", "Line", "Thirty Two", "Leki", "Komperdell", "Kerma"];
var bindingBrands = ["Rossignol", "Nordica", "Salomon", "Fischer", "Stoeckli", "Voelkl", "Blizzard", "Dynastar", "Madschus", "Burton", "Atomic", "Head", "K2", "Technica", "Dynastar", "Lib Tech", "Gnu", "Union", "Liberty", "Marker", "Tyrolia", "Flux", "Burton", "Armada", "Fischer", "Capita", "Never Summer", "Line", "Thirty Two", "Leki", "Komperdell", "Kerma"];
var cities = ["Seattle","Shoreline","Edmonds","Mountlake Terrace","Redmond","Renton","Olympia","Tacoma","Lynnwood","Everett","Bothel","Brier","Kirkland","Redmond","Magnolia","Ravenna","Lake City","Greenwood","Lake Forest Park","Woodinville","Ballard","Bellingham","Mukilteo","Bellevue"];

$(".skiComplete").suggest(skiBrands);
$(".cityInput").suggest(cities);
//function assignMask(elem,mod){
//    document.getElementById(elem).addEventListener('input', maskInput(elem, input.value, mod),false);
//}

//function maskInput(elem, input, mod) {
//    var old = document.getElementById(elem);
//    var mask = input + mod;
//    old.value = mask;
//}

function inputLinkValidation(primary, secondary) {
    console.log("here");
        var prime = document.getElementById(primary);
        var second = document.getElementById(secondary);
        //console.log("test");
        if (prime.value != "") {
            if (second.value == "") {
                //add error
                document.getElementById(primary + "Validation").innerHTML = "Please enter a Size.";
                formLinkValidation();
                return;
            }
            else {
                if (second.value == "cm") {
                    document.getElementById(primary + "Validation").innerHTML = "Please enter a Size.";
                    formLinkValidation();
                    return;
                }
                if (second.value == "in") {
                    document.getElementById(primary + "Validation").innerHTML = "Please enter a Size.";
                    formLinkValidation();
                    return;
                }
                if (second.value == "mm") {
                    document.getElementById(primary + "Validation").innerHTML = "Please enter a Size.";
                    formLinkValidation();
                    return;
                }
                document.getElementById(primary + "Validation").innerHTML = "";
                formLinkValidation();
                return;
            }
        }
        else {
            if (second.value != "") {
                if (prime.value == "") {
                    //add error
                    if (second.value != "cm") {
                        if (second.value != "in") {
                            if (second.value != "mm") {
                                document.getElementById(primary + "Validation").innerHTML = "Please enter a Brand.";
                                formLinkValidation();
                                return;
                            }
                        }
                    }
                    else {
                        document.getElementById(primary + "Validation").innerHTML = "";
                        formLinkValidation();
                        return;
                    }
                }
                else {
                    document.getElementById(primary + "Validation").innerHTML = "";
                    formLinkValidation();
                    return;
                }
            }
        }
        document.getElementById(primary + "Validation").innerHTML = "";
        formLinkValidation();
}

function formLinkValidation() {
    var links = document.getElementsByName("LinkValidation");
    var errors = 0;
    for (var x = 0; x < links.length; x++) {
        if (links[x].innerText != "") { errors += 1;}
    }
    if (errors > 0) {
        document.getElementById("createButton").setAttribute("disabled", "true");
        if (document.getElementById("attatchLink") != null){
            document.getElementById("attatchLink").setAttribute("style", "pointer-events:none;");
        }
    }
    else {
        if (document.getElementById("attatchLink") != null) {
            document.getElementById("attatchLink").removeAttribute("style");
        }
        document.getElementById("createButton").removeAttribute("disabled");
    }
}

function CreateSetValidation(security) {
    var errors = 0;
    if (document.getElementById("FirstName").value == "") { errors += 1; }
    if (document.getElementById("LastName").value == "") { errors += 1; }
    if (document.getElementById("PhoneNumber").value == "") { errors += 1; }
    if (document.getElementById("PhoneNumber2").value == "") { errors += 1; }
    if (document.getElementById("Address").value == "") { errors += 1; }
    if (document.getElementById("City").value == "") { errors += 1; }
    if (document.getElementById("State").value == "") { errors += 1; }
    if (document.getElementById("PostalCode").value == "") { errors += 1; }
    if (document.getElementById("EmailAddress").value == "") { errors += 1; }
    if (security) {
        if (document.getElementById("Height").value == "") { errors += 1; }
        if (document.getElementById("Weight").value == "") { errors += 1; }
        if (document.getElementById("Age").value == "") { errors += 1; }
    }
    if (errors > 0) {
        //disable submit, redirect to form1 submit
        document.getElementById("attatchLink").setAttribute("data-validated", "false");
    }
    else {
        //enable submit
        document.getElementById("attatchLink").setAttribute("data-validated","true");
    }
}

function chevronSwap(elem,elem2) {
    var icon = document.getElementById(elem + "Chevron");
    if (icon.classList.toString().indexOf("up") == -1) {
        icon.classList = "glyphicon glyphicon-chevron-up";
    }
    else {
        icon.classList = "glyphicon glyphicon-chevron-down";
    }
    if (elem2 != null) {
        var icon2 = document.getElementById(elem2 + "Chevron");
        if (icon2.classList.toString().indexOf("up") == -1) {
            icon2.classList = "glyphicon glyphicon-chevron-up";
            return false;
        }
        else {
            icon2.classList = "glyphicon glyphicon-chevron-down";
            return false;
        }
    }
}

function returnModal(first, last, id) {
    sessionStorage.setItem("ReturnFirst",first);
    sessionStorage.setItem("ReturnLast",last);
    sessionStorage.setItem("ReturnId",id);
}

//store filter option
function storeFilter() {
    sessionStorage.setItem("report", document.getElementById("filterSelect").value);
}
function readFilter() {
    if (sessionStorage.getItem('report') != null && sessionStorage.getItem('report') != 'undefined') {
        document.getElementById('filterSelect').value = sessionStorage.getItem('report');
    }
}


// Reports Print Methods
function printReport() {
    var toHide = document.getElementsByClassName("printHide");
    for (var x = 0; x < toHide.length; x++) {
        toHide[x].setAttribute("style","display:none;");
    }
    window.print();
    for (var x = 0; x < toHide.length; x++) {
        toHide[x].setAttribute("style", "");
    }
}

function printFullReport() {
    var toHide = document.getElementsByClassName("printHide3");
    for (var x = 0; x < toHide.length; x++) {
        toHide[x].setAttribute("style", "display:none;");
    }
    window.print();
    for (var x = 0; x < toHide.length; x++) {
        toHide[x].setAttribute("style", "");
    }
}

function printRentalSet(bool) {
    var toHide = document.getElementsByClassName("printHide");
    for (var x = 0; x < toHide.length; x++) {
        toHide[x].setAttribute("style", "display:none;");
    }
    if (bool) {
        var obj = document.getElementsByClassName("rentalBody");
        var spacer = document.getElementsByClassName("spacer");
        for (var x = 0; x < obj.length; x++){
            //spacer[x].setAttribute("style", "height:10in;");
            if (x == (obj.length - 1)) {
                obj[x].setAttribute("style", "height:14in");
            }
            else {
                obj[x].setAttribute("style","height:15in");
            }
            //console.log(obj[x].clientHeight);
        }
    }
    window.print();
    for (var x = 0; x < toHide.length; x++) {
        toHide[x].setAttribute("style", "");
    }
    if (bool) {
        var obj = document.getElementsByClassName("rentalBody");
        var spacer = document.getElementsByClassName("spacer");
        for (var x = 0; x < obj.length; x++) {
            //spacer[x].setAttribute("style", "height:10in;");
            obj[x].setAttribute("style", "");
            //console.log(obj[x].clientHeight);
        }
    }
}

function printUnreturned() {
    var toHide = document.getElementsByClassName("printHide2");
    for (var x = 0; x < toHide.length; x++) {
        toHide[x].setAttribute("style", "display:none;");
    }
    window.print();
    for (var x = 0; x < toHide.length; x++) {
        toHide[x].setAttribute("style", "");
    }
}

// not used section
function gatherData(){
    var data = {
        ID : $('#ID'),
        FirstName : $('#FirstName').val(),
        LastName : $('#LastName').val(),
        EmailAddress : $('#EmailAddress').val(),
        Address : $('#Address').val(),
        City : $('#City').val(),
        State : $('#State').val(),
        PhoneNumber : $('#PhoneNumber').val(),
        PhoneNumber2 : $('#PhoneNumber2').val(),
        LocalAccommodations : $('#LocalAccommodations').val(),
        Height : $('#Height').val(),
        Weight : $('#Weight').val(),
        SkierType : $('#SkierType').val(),
        SkiBrand : $('#SkiBrand').val(),
        SkiSize : $('#SkiSize').val(),
        BoardBrand : $('#BoardBrand').val(),
        BoardStance : $('#BoardStance').val(),
        BoardSize : $('#BoardSize').val(),
        StanceAngles : $('#StanceAngles').val(),
        PoleSize : $('#PoleSize').val(),
        PoleBrand : $('#PoleBrand').val(),
        BindingSize : $('#BindingSize').val(),
        BindingBrand : $('#BindingBrand').val(),
        Age : $('#Age').val(),
        Sex : $('#Sex').val(),
        CustomerNotes : $('#CustomerNotes').val(),
        Returned : $('#Returned').val(),
        PublicSubmission : $('#PublicSubmission').val(),
        BootBrand : $('#BootBrand').val(),
        BootSize : $('#BootSize').val(),
        IdNext : $('#IdNext').val(),
        IdPrevious : $('#IdPrevious').val(),
        DailyRental : $('#FirstName').val()
        }
        return data;
    }
function create2() {

        $.ajax({
            type: 'post',
            url: 'Rentals/Create2/',
            data: {
            ID : $('#ID'),
        FirstName : $('#FirstName').value,
        LastName : $('#LastName').value,
        EmailAddress : $('#EmailAddress').value,
        Address : $('#Address').value,
        City : $('#City').value,
        State : $('#State').value,
        PhoneNumber : $('#PhoneNumber').value,
        PhoneNumber2 : $('#PhoneNumber2').value,
        LocalAccommodations : $('#LocalAccommodations').value,
        Height : $('#Height').value,
        Weight : $('#Weight').value,
        SkierType : $('#SkierType').value,
        SkiBrand : $('#SkiBrand').value,
        SkiSize : $('#SkiSize').value,
        BoardBrand : $('#BoardBrand').value,
        BoardStance : $('#BoardStance').value,
        BoardSize : $('#BoardSize').value,
        StanceAngles : $('#StanceAngles').value,
        PoleSize : $('#PoleSize').value,
        PoleBrand : $('#PoleBrand').value,
        BindingSize : $('#BindingSize').value,
        BindingBrand : $('#BindingBrand').value,
        Age : $('#Age').value,
        Sex : $('#Sex').value,
        CustomerNotes : $('#CustomerNotes').value,
        Returned : $('#Returned').value,
        PublicSubmission : $('#PublicSubmission').value,
        BootBrand : $('#BootBrand').value,
        BootSize : $('#BootSize').value,
        IdNext : $('#IdNext').value,
        IdPrevious : $('#IdPrevious').value,
        DailyRental : $('#FirstName').value
            },
            timeout: 5000,
            datatype: 'json'
        })
    }

function maxLengthCheck(object) {
    if (object.value.length > object.max.length)
        object.value = object.value.slice(0, object.max.length)
}

function isNumeric(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    var regex = /[0-9]|\./;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}
