﻿var validation = (function () {
    // Private Namespace
    
    // Attatch AutoComplete
    //$('.citySuggest').suggest(citySuggest);
    //$('.skiSuggest').suggest(skiBrands);

    // Attatch Measurement-Complete
    $("#ski-size").on("blur", function () { affixToString("ski-size", "cm"); });
    $("#pole-size").on("blur", function () { affixToString("pole-size", "in"); });
    $("#boot-sole").on("blur", function () { affixToString("boot-sole", "mm"); });

    // DOM ID's for reference
    var tInputs = ["Email", "PNumber", "PNumber2"];
    var sInputs = ["ski-brand","ski-size","ski-inventory","pole-brand","pole-size","pole-inventory","binding-brand","binding-size","binding-inventory"]
    var citySuggest = ["Seattle", "Shoreline", "Edmonds", "Mountlake Terrace", "Redmond", "Renton", "Olympia", "Tacoma", "Lynnwood", "Everett", "Bothel", "Brier", "Kirkland", "Redmond", "Magnolia", "Ravenna", "Lake City", "Greenwood", "Lake Forest Park", "Woodinville", "Ballard", "Bellingham", "Mukilteo", "Bellevue"];
    var skiBrands = ["Rossignol", "Nordica", "Salomon", "Fischer", "Stoeckli", "Volkl", "Blizzard", "Dynastar", "Madschus", "Burton", "Atomic", "Head", "K2", "Tecnica", "Dynastar", "Lib Tech", "Gnu", "Union", "Liberty", "Marker", "Tyrolia", "Flux", "Burton", "Armada", "Fischer", "Capita", "Never Summer", "Line", "Thirty Two", "Leki", "Komperdell", "Kerma", "Swix", "Scott", "Dalbello", "Lange", "Excite"];
    var tooltip = [false, false, false];
    var usa = true;
    var Imperial = false;

    // DOM Tab Index's for reference
    var sIndex = {
        skibrand: $("#ski-brand"),
        skisize: $("#ski-size"),
        skiinventory: $("#ski-inventory"),
        polebrand: $("#pole-brand"),
        polesize: $("#pole-size"),
        poleinventory: $("#pole-inventory"),
        bindingbrand: $("#binding-brand"),
        bindingsize: $("#binding-size"),
        bindinginventory: $("#binding-inventory"),
        boardbrand: $("#board-brand"),
        boardsize: $("#board-size"),
        boardinventory: $("#board-inventory"),
        boardbindingbrand: $("#boardbinding-brand"),
        boardbindingsize: $("#boardbinding-size"),
        boardbindinginventory: $("#boardbinding-inventory"),
        boardstance: $("#board-stance"),
        stanceangles: $("#stance-angles"),
        bootbrand: $("#boot-brand"),
        bootsize: $("#boot-size"),
        bootinventory: $("#boot-inventory"),
        bootsole: $("#boot-sole")
    }


    function sLogic(value, id, global) {
            var globalCounter=0;
            var modalBtn = "";
            if (id.indexOf("ski") != -1 || id.indexOf("pole") != -1 || id.indexOf("binding") == 0) { modalBtn = "swapL2"; } else { modalBtn = "swapL7"; }
            if (id.indexOf("boot") != -1) { modalBtn = "swapL22"; }
            var pagegrp = modalBtn == "swapL2" ? ["req3", "req4", "req5"] : ["req7", "req8"];
            if (modalBtn == "swapL22") { pagegrp = ["req6"];}
            if (!global) {
                if (value == "") {
                    document.getElementById(id).classList += " req-invalid";
                    document.getElementById(modalBtn).setAttribute("disabled", "disabled");
                }
                else {
                    document.getElementById(id).classList.remove("req-invalid");
                    pagegrp.forEach(init);
                    if (globalCounter > 0) { document.getElementById(modalBtn).setAttribute("disabled", "disabled"); }
                    else { document.getElementById(modalBtn).removeAttribute("disabled"); }
                }
            }
            else {
                pagegrp.forEach(init);
                if (globalCounter > 0) { document.getElementById(modalBtn).setAttribute("disabled", "disabled"); }
                else { document.getElementById(modalBtn).removeAttribute("disabled"); }
            }
        function init(item,index) {
            var req_grp = document.getElementsByClassName(item);
            var fill = 0;
            var empty = 0;
            for (var x = 0; x < req_grp.length; x++) { req_grp[x].value == "" ? empty++ : fill++; }
            if (req_grp.length == fill) { return; }
            if (req_grp.length == empty) { return; }
            globalCounter++;
        }
        //dCheck(id, skip);
    }

    // Find What Modal Is In Use
    function dCheck(id,skip) {
        if (skip != true) {
            //grab the parent modal div
            var modal = $("#" + id).parents(".modal").first();

            switch (modal.attr("id")) {
                case "modal1":
                    // First page of the modal
                    dLogic("req1", "swapR1");
                    break;
                case "modal2":
                    // Second page of the modal
                    dLogic("req2", "swapSki");
                    break;
                case "modal3":
                    if (id.indexOf("ski") != -1) {
                        dLogic("req3", "swapL2", true);
                        break;
                    }
                    if (id.indexOf("pole") != -1) {
                        dLogic("req4", "swapL2",true);
                        break;
                    }
                    if (id.indexOf("binding") != -1) {
                        dLogic("req5", "swapL2",true);
                        break;
                    }
                    break;
                case "modal4":
                    if (id.indexof("board-") != -1) {
                        dLogic("req7","swapL7");
                    }
                    else {
                        dLogic("req8", "swapL7");
                    }
                    break;
                case "modal5":
                    dLogic("req6","swapL22")
                    break;
            }
        }
    }

    // Enable or Disable Next Button
    function dLogic(group,buttonId) {
        var reqgrp = document.getElementsByClassName(group);
        var counter = 0;
        for (var x = 0; x < reqgrp.length; x++) {
            if (reqgrp[x].value != "") { counter++; }
        }
        if (reqgrp.length > counter) {
            document.getElementById(buttonId).setAttribute("disabled", "disabled");
            if (buttonId == "swapSki") { document.getElementById("swapSnow").setAttribute("disabled", "disabled"); }
        }
        else {
            var grp = document.getElementsByClassName("grp1");
            var skip = false;
            for (var y = 0; y < grp.length; y++) {
                if (grp[y].getAttribute("data-invalid") != null) {
                    skip = true;
                }
            }
            if (!skip) {
                document.getElementById(buttonId).removeAttribute("disabled");
                if (buttonId == "swapSki") { document.getElementById("swapSnow").removeAttribute("disabled"); }
            };
        }
    }


    // Decide Tooltip Message Dependent on Error Code
    function tLogic(id, code) {
        switch (code) {
            case 0:
                $("#" + id).tooltip({ title: "Invalid Email Address!", trigger: "manual" });
                $("#" + id).on("shown.bs.tooltip", () => { tooltip[2] = true; })
                if (!tooltip[2]) {
                    $("#" + id).tooltip("show");
                }
                break;
            case 1:
                var toggle = id == "PNumber" ? 0 : 1;
                $("#" + id).tooltip({ title: "Numbers Only!", trigger: "manual" });
                $("#" + id).on("shown.bs.tooltip", () => { tooltip[toggle] = true; })
                if (!tooltip[toggle]) {
                    $("#" + id).tooltip("show");
                }
                break;
            case 2:
                $("#" + id).tooltip({ title: "Add a Size!", trigger: "manual" });
                $("#" + id).addClass("req-invalid");
                $("#" + id).tooltip("show");
            case 3:
                $("#" + id).tooltip({ title: "Add a Brand!", trigger: "manual" });
                $("#" + id).addClass("req-invalid");
                $("#" + id).tooltip("show");
        }
    }

    // alter tab index according to the open feilds 
    function alter_sIndex(type,toggle) {
        const t = "tabindex";
        if (toggle) {
            switch (type) {
                case "ski":
                    sIndex.skiinventory.attr(t, (sIndex.skisize.attr(t) + 1));
                    addOne(sIndex.polebrand);
                    addOne(sIndex.polesize);
                    if (sIndex.poleinventory.attr("tabindex") > -1) { addOne(sIndex.poleinventory); };
                    addOne(sIndex.bindingbrand);
                    addOne(sIndex.bindingsize);
                    if (sIndex.bindinginventory > -1) { addOne(sIndex.bindinginventory); };
                    //sIndex.set();
                    break;
                case "pole":
                    sIndex.poleinventory.attr(t,(sIndex.polesize.attr(t) + 1));
                    addOne(sIndex.bindingbrand);
                    addOne(sIndex.bindingsize);
                    if (sIndex.bindinginventory > -1) { addOne(sIndex.bindinginventory); };
                    //sIndex.set();
                    break;
                case "binding":
                    sIndex.bindinginventory.attr(t,(sIndex.bindingsize.attr(t) + 1));
                    //sIndex.set();
                    break;
                case "board":
                    sIndex.boardinventory.attr(t,(sIndex.boardsize.attr(t) + 1));
                    addOne(sIndex.boardbindingbrand);
                    addOne(sIndex.boardbindingsize);
                    if (sIndex.boardbindinginventory > -1) { addOne(sIndex.boardbindinginventory); };
                    addOne(sIndex.boardstance);
                    addOne(sIndex.stanceangles); 
                    //sIndex.set();
                    break;
                case "boardbinding":
                    sIndex.boardbindinginventory.attr(t,(sIndex.boardbindingsize.attr(t) + 1));
                    addOne(sIndex.boardstance);
                    addOne(sIndex.stanceangles);
                    //sIndex.set();
                    break;
                case "boot":
                    sIndex.bootinventory.attr(t,(sIndex.bootsole.attr(t) + 1));
                    //sIndex.set();
            }
            return;
        }
        else {
            switch (type) {
                case "ski":
                    sIndex.skiinventory.attr(t,-1);
                    takeOne(sIndex.polebrand);
                    takeOne(sIndex.polesize);
                    if (sIndex.poleinventory > -1) { takeOne(sIndex.poleinventory); };
                    takeOne(sIndex.bindingbrand);
                    takeOne(sIndex.bindingsize);
                    if (sIndex.bindinginventory > -1) { takeOne(sIndex.bindinginventory); };
                    break;
                case "pole":
                    sIndex.poleinventory.attr(t,-1);
                    takeOne(sIndex.bindingbrand);
                    takeOne(sIndex.bindingsize);
                    if (sIndex.bindinginventory > -1) { takeOne(sIndex.bindinginventory); };
                    takeOne(sIndex.boardstance);
                    takeOne(sIndex.stanceangles);
                    break;
                case "binding":
                    sIndex.bindinginventory.attr(t,-1);
                    break;
                case "board":
                    sIndex.boardinventory.attr(t,-1);
                    takeOne(sIndex.boardbindingbrand);
                    takeOne(sIndex.boardbindingsize);
                    if (sIndex.boardbindinginventory > -1) { takeOne(sIndex.boardbindinginventory); };
                    takeOne(sIndex.boardstance);
                    takeOne(sIndex.stanceangles);
                    break;
                case "boardbinding":
                    sIndex.boardbindinginventory.attr(t,-1);
                    takeOne(sIndex.boardstance);
                    takeOne(sIndex.stanceangles);
                    break;
                case "boot":
                    sIndex.bootinventory.attr(t,-1);
                    takeOne(sIndex.bootsole);
                    break;
            }
            return;
        }
        function addOne(element) {
            element.attr("tabindex", (element.attr("tabindex") + 1));
        }
        function takeOne(element) {
            element.attr("tabindex", (element.attr("tabindex") -  1));
        }
    }

    function affixToString(elem, mod) {
        console.log("elem: " + elem + " mod: " + mod);
        var old = document.getElementById(elem.toString());
        var oldValue = old.value;
        if (oldValue.indexOf(mod) == -1) {
            if (oldValue != "") {
                old.value = oldValue + mod;
                return;
            }
        }
    }


    function rCheck(value, id) {
                var patt = /[a-z]/;
        switch (id) {
            case "Email":
                var patt1 = /.+\@.+\..+/;
                if (!patt1.test(value)) {
                    tLogic(id, 0);
                    $("#"+id).attr("data-invalid","true");
                    $("#swapR1").attr("disabled", "disabled");
                }
                else {
                    $("#" + id).tooltip("destroy");
                    tooltip[2] = false;
                    $("#"+id).removeAttr("data-invalid");
                    dLogic("req1", "swapR1");
                }
                break;
            case "PNumber":
                if (patt.test(value)) {
                    tLogic(id, 1);
                    $("#"+id).attr("data-invalid", "true");
                    $("#swapR1").attr("disabled", "disabled");
                }
                else {
                    $("#" + id).tooltip("destroy");
                    tooltip[0] = false;
                    $("#"+id).removeAttr("data-invalid");
                    dLogic("req1", "swapR1");
                }
                break;
            case "PNumber2":
                // Not Required, but if entered incorrectly disable button
                if (patt.test(value)) {
                    tLogic(id, 1);
                    $("#"+id).attr("data-invalid", "true");
                    $("#swapR1").attr("disabled", "disabled");
                }
                else {
                    tooltip[1] = false;
                    $("#" + id).tooltip("destroy");
                    $("#"+id).removeAttr("data-invalid");
                    dLogic("req1", "swapR1");
                }
                break;
        }
    }


    function destroyTooltip(sel1, sel2) {    
        $("#" + sel1).tooltip("destroy");
        $("#" + sel2).tooltip("destroy");
     }

    function toggleCollapse(id, trigger) {
         toggle = trigger == true ? "show" : "hide";
         if (id == "boot-brand-collapse") {
             if ($("#boot-inventory").val() == "" && toggle == "hide") {
                 $("#" + id).collapse(toggle);
             }
             if ($("#boot-inventory").val() == "" && toggle == "show") {
                 $("#" + id).collapse(toggle);
             }
         }
         else {
            $("#" + id).collapse(toggle);
         }
         if (toggle == "show") {
             if (id == "boot-brand-collapse") {
                 if ($("#boot-inventory").val() == "") {
                    cancelCollapse(id); $("#" + (id.slice(0, id.indexOf("-")) + "-inventory")).addClass("req-invalid");
                 }
             }
             else{
                cancelCollapse(id); $("#" + (id.slice(0, id.indexOf("-")) + "-inventory")).addClass("req-invalid");
             }
         }
    }

    function cancelCollapse(id) {
        var check = setInterval(function () {
            if ($("#" + id).hasClass("collapsing")) { console.log("true"); } else { doubleCheck(); }
        }, 100);
        function doubleCheck() {
            clearInterval(check);
            var type = id.slice(0, id.indexOf("-"));
            if ($("#" + type + "-brand").val() == "" && $("#" + type + "-size").val() == "" && $("#" + type + "-inventory").val() == "") {
                $("#" + id).collapse("hide");
            }
        }
    }

    function postServer(rental) {
        $.post("Rentals/Create", rental, function (data) {
            console.log(data);
            window.location.href = data.url;
            data.err == true ? sessionStorage.setItem("err", "true") : sessionStorage.setItem("err", "false");
            $("#finishUp").removeAttr("disabled");
        });
    }

    function postServerSet(rental) {
        $.post("Rentals/Create2", rental, function (res) {
            console.log(res);
            $("#IdPrevious").val(res.rental.idPrevious);
            $("#modal6").modal("hide");
            //Clear out personal info
            $("#Height").val("");
            $("#Weight").val("");
            $("#Age").val("");
            $("#ski-brand").val("");
            $("#ski-size").val("");
            $("#ski-inventory").val("");
            $("#pole-brand").val("");
            $("#pole-size").val("");
            $("#pole-inventory").val("");
            $("#binding-brand").val("");
            $("#binding-size").val("");
            $("#binding-inventory").val("");
            $("#board-brand").val("");
            $("#board-size").val("");
            $("#board-inventory").val("");
            $("#boardbinding-brand").val("");
            $("#boardbinding-size").val("");
            $("#boardbinding-inventory").val("");
            $("#board-stance").val("");
            $("#stance-angles").val("");
            $("#boot-brand").val("");
            $("#boot-size").val("");
            $("#boot-inventory").val("");
            $("#boot-sole").val("");
            $("#customerNotes").val("");
            $("#skiType1").addClass("btn-primary"); 
            $("#skiType2").removeClass("btn-primary");
            $("#skiType3").removeClass("btn-primary");
            $("#skiType4").removeClass("btn-primary");
            //Clear out personal info
            //Run validation one more time
            $("#Height")[0].oninput();
            $("#Weight")[0].oninput();
            $("#Age")[0].oninput();
            $("#ski-brand")[0].oninput();
            $("#ski-size")[0].oninput();
            $("#ski-inventory")[0].oninput();
            $("#pole-brand")[0].oninput();
            $("#pole-size")[0].oninput();
            $("#pole-inventory")[0].oninput();
            $("#binding-brand")[0].oninput();
            $("#binding-size")[0].oninput();
            $("#binding-inventory")[0].oninput();
            $("#board-brand")[0].oninput();
            $("#board-size")[0].oninput();
            $("#board-inventory")[0].oninput();
            $("#boardbinding-brand")[0].oninput();
            $("#boardbinding-size")[0].oninput();
            $("#boardbinding-inventory")[0].oninput();
            $("#boot-brand")[0].oninput();
            $("#boot-size")[0].oninput();
            $("#boot-inventory")[0].oninput();
            //Run validation one more time
            $("#modal1").modal("show");
            $("#finishUp").text("Save and Finish");
            $("#finishUp").attr("onclick", "validation.createRental('finish')");
            $("#createSet2").text("Save and Continue");
            if (res.create == "true") {
                setSnackbar(false);
                updateRentalSetCounter(true,getCookie("SetCount").toString())
            }
            else { setSnackbar(true); }
            $("#finishUp").removeAttr("disabled");
        });
    }

    function postServerSetFinal(rental) {
        $.post("Rentals/Create3", rental, function (res) {
            console.log(res);
            if (!res.status == "success") { 
                sessionStorage.setItem("err", "true");
            }
            else {
                sessionStorage.setItem("err", "false");
                updateRentalSetCounter(false);
                window.location.href = "/Rentals";
            }
            $("#finishUp").removeAttr("disabled");
        });
    }

    function setSnackbar(status) {
        status == true ? $("#snackbar").text("Error Saving!") : $("#snackbar").text("Rental Added!")
        $("#snackbar").addClass("show");
        // After 3 seconds, remove the show class from DIV
        setTimeout(function () { $("#snackbar").removeClass("show"); }, 3000);
        sessionStorage.removeItem("err");
    }

    function updateRentalSetCounter(status, count) {
        let counter = $(".modal-set-counter");
        console.log(counter);
        if (status) {
            for (var x = 0; x < counter.length; x++) {
                counter[x].innerText = "Rental #" + count + " in Set";
            }
        }
        else {
            for (var y = 0; y < counter.length; y++) {
                counter[y].innerText = "";
            }
        }
    }

    function getCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
    }

    function createSetMode() {
        $("#modal1").modal("show");
        $("#finishUp").text("Save and Finish");
        $("#finishUp").attr("onclick", "validation.createRental('finish')");
        $("#createSet2").text("Save and Continue");
    }

    //End Private Namespace
    return {
        // Public Namespace
        citySuggest : ["Seattle", "Shoreline", "Edmonds", "Mountlake Terrace", "Redmond", "Renton", "Olympia", "Tacoma", "Lynnwood", "Everett", "Bothel", "Brier", "Kirkland", "Redmond", "Magnolia", "Ravenna", "Lake City", "Greenwood", "Lake Forest Park", "Woodinville", "Ballard", "Bellingham", "Mukilteo", "Bellevue"],
        skiBrands : ["Rossignol", "Nordica", "Salomon", "Fischer", "Stoeckli", "Volkl", "Blizzard", "Dynastar", "Madschus", "Burton", "Atomic", "Head", "K2", "Tecnica", "Dynastar", "Lib Tech", "Gnu", "Union", "Liberty", "Marker", "Tyrolia", "Flux", "Burton", "Armada", "Fischer", "Capita", "Never Summer", "Line", "Thirty Two", "Leki", "Komperdell", "Kerma", "Swix", "Scott", "Dalbello", "Lange", "Excite"],

        snackbarStatus: function (status) {
            status == true ? $("#snackbar").text("Error Saving!") : $("#snackbar").text("Rental Added!")
            $("#snackbar").addClass("show");
            // After 3 seconds, remove the show class from DIV
            setTimeout(function () { $("#snackbar").removeClass("show"); }, 3000);
            sessionStorage.removeItem("err");
        },
        // Add or Remove required properties to equipment
        sCheck: function (value, id) {
            var type = id.slice(0, (id.indexOf("-")));
            var brand = "";
            var size = "";
            var reqgrp = "";
            switch (type) {
                case "ski":
                    brand = "ski-brand";
                    size = "ski-size";
                    reqgrp = "req3";
                    break;
                case "pole":
                    brand = "pole-brand";
                    size = "pole-size";
                    reqgrp = "req4";
                    break;
                case "binding":
                    brand = "binding-brand";
                    size = "binding-size";
                    reqgrp = "req5";
                    break;
                case "boot":
                    brand = "boot-brand";
                    size = "boot-size";
                    reqgrp = "req6";
                    break;
                case "board":
                    brand = "board-brand";
                    size = "board-size";
                    reqgrp = "req7";
                    break;
                case "boardbinding":
                    brand = "boardbinding-brand";
                    size = "boardbinding-size";
                    reqgrp = "req8";
                    break;
            }
            if ($("#" + type + "-inventory").val() != "" || $("#" + brand).val() != "" || $("#" + size).val() != "") {
                // one of the boxes has text in it
                if (!$("#" + brand).hasClass(reqgrp)) { document.getElementById(brand).classList += " "+reqgrp;  }
                if (!$("#" + size).hasClass(reqgrp)) { document.getElementById(size).classList += " "+reqgrp; }
                if (!$("#" + type + "-inventory").hasClass(reqgrp)) { document.getElementById(type + "-inventory").classList += " " + reqgrp; }
                sLogic(value, id,false);
            }
            else {
                if ($("#" + brand).hasClass(reqgrp)) { $("#" + brand).removeClass(reqgrp).removeClass("req-invalid"); }
                if ($("#" + size).hasClass(reqgrp)) { $("#" + size).removeClass(reqgrp).removeClass("req-invalid"); }
                if ($("#" + type + "-inventory").hasClass(reqgrp)) { $("#" + type + "-inventory").removeClass(reqgrp).removeClass("req-invalid"); }
                if (type == "boot") { toggleCollapse("boot-brand-collapse",false); }
                sLogic(value, id, true);
            }
        },
        // Add or Remove Invalid Class
        vCheck: function (value, id) {
            var skip = false;
            if (value == "") {
                document.getElementById(id).classList += " req-invalid";
                if (tInputs.indexOf(id) != -1) { rCheck(value, id); skip = true; }
            }
            else {
                document.getElementById(id).classList.remove("req-invalid");
                if (tInputs.indexOf(id) != -1) { rCheck(value, id); skip = true; }
            }
            dCheck(id,skip);
        },
        // Check if a equipment line is partially filled out
        eCheck: function (id, value) {
            var type = id.slice(0, (id.indexOf("-")));
            var brand = "";
            var size = "";
            switch (type) {
                case "ski":
                    brand = "ski-brand";
                    size = "ski-size";
                    break;
                case "pole":
                    brand = "pole-brand";
                    size = "pole-size";
                    break;
                case "binding":
                    brand = "binding-brand";
                    size = "binding-size";
                    break;
                case "boot":
                    brand = "boot-brand";
                    size = "boot-size";
                    break;
                case "board":
                    brand = "board-brand";
                    size = "board-size";
                    break;
                case "boardbinding":
                    brand = "boardbinding-brand";
                    size = "boardbinding-size";
                    break;
            }

            if (value != "") {
                if (id == brand) {
                    if (document.getElementById(size).value == "") { tLogic(size, 2); toggleCollapse(brand + "-collapse", true); alter_sIndex(type, true); } else { destroyTooltip(size, brand); }
                }
                else {
                    if (document.getElementById(brand).value == "") { tLogic(brand, 3); toggleCollapse(brand + "-collapse", true); alter_sIndex(type, true); } else { destroyTooltip(size, brand); }
                };
            }
            else {
                if (id == brand) {
                    if (document.getElementById(size).value != "") { tLogic(brand, 2); toggleCollapse(brand + "-collapse", true); alter_sIndex(type, true); } else { destroyTooltip(size, brand); toggleCollapse(brand + "-collapse", false); alter_sIndex(type, false) }
                }
                else {
                    if (document.getElementById(brand).value != "") { tLogic(size, 2); toggleCollapse(brand + "-collapse", true); alter_sIndex(type, true); } else { destroyTooltip(size, brand); toggleCollapse(brand + "-collapse", false); alter_sIndex(type, false) }
                }
            }

        },
        // Swap colors for ski-type button UI
        btnSelect: function(id) {
         if (!$("#skiType" + id).hasClass("btn-primary")) { $("#skiType" + id).addClass("btn-primary"); }
         for (var x = 0; x < 4; x++) {
             if ((x + 1) != parseInt(id)) {
                 $("#skiType" + (x+1)).removeClass("btn-primary");
             }
         }
     },
        // Run Special Check on Special Inputs
        rCheck: function (value, id) {
        var patt = /[a-z]/;
        switch (id) {
            case "Email":
                var patt1 = /.+\@.+\..+/;
                if (!patt1.test(value)) {
                    tLogic(id, 0);
                    $("#"+id).attr("data-invalid","true");
                    $("#swapR1").attr("disabled", "disabled");
                }
                else {
                    $("#" + id).tooltip("destroy");
                    $("#"+id).removeAttr("data-invalid");
                    dLogic("req1", "swapR1");
                }
                break;
            case "PNumber":
                if (patt.test(value)) {
                    tLogic(id, 1);
                    $("#"+id).attr("data-invalid", "true");
                    $("#swapR1").attr("disabled", "disabled");
                }
                else {
                    $("#" + id).tooltip("destroy");
                    tooltip[0] = false;
                    $("#"+id).removeAttr("data-invalid");
                    dLogic("req1", "swapR1");
                }
                break;
            case "PNumber2":
                // Not Required, but if entered incorrectly disable button
                if (patt.test(value)) {
                    tLogic(id, 1);
                    $("#"+id).attr("data-invalid", "true");
                    $("#swapR1").attr("disabled", "disabled");
                }
                else {
                    tooltip[1] = false;
                    $("#" + id).tooltip("destroy");
                    $("#"+id).removeAttr("data-invalid");
                    dLogic("req1", "swapR1");
                }
                break;
        }
    },
        swapBackButton: function(type) {
        switch (type) {
            case "ski":
                $('#swapR32').off("click");
                $('#swapR32').on("click", () => { $("#modal3").modal('show'); $("#modal5").modal('hide'); });
                break;
            case "snow":
                $("#swapR32").off("click");
                $('#swapR32').on("click", () => { $("#modal4").modal('show'); $("#modal5").modal('hide'); });
                break;
        }
        },
        measure: function (id) {
            var height = $("#Height");
            var weight = $("#Weight");
            if (id == 0) {
                height.attr("placeholder", "Height (ft'in\")");
                weight.attr("placeholder", "Weight (lb.)");
                usa = true;
                if (height.val() !== "") { this.measureComplete("Height", height.val(),true); }
                if (weight.val() !== "") { this.measureComplete("Weight", weight.val(),true); }
            }
            else {
                height.attr("placeholder", "Height (cm.)");
                weight.attr("placeholder", "Weight (kg.)");
                usa = false;
                if (height.val() !== "") { this.measureComplete("Height", height.val(),true); }
                if (weight.val() !== "") { this.measureComplete("Weight", weight.val(),true); }
            }
        },
        measureComplete: function (id, value, convert) {
            var inches;
            if (value != "") {
                if (usa) {
                    if (id == "Weight")
                    {
                            
                        if (convert)
                        {
                            if (value.indexOf("lb") == -1) {
                                var v = parseInt(value.replace("kg", "")); $('#' + id).val(Math.round((v * 2.2)) + "lb");
                            }
                        }
                        else{
                            if (value.indexOf("lb") == -1) { $("#" + id).val(value + "lb"); }
                        }
                    }
                    if (id == "Height") {
                        if (convert) {
                            if (value.indexOf("\"") == -1) {
                                var z = parseInt(value.replace("cm", ""));
                                inches = Math.round(z * 0.3937);
                                var feet = Math.floor(inches / 12);
                                var _inches = (inches - (feet * 12));
                                $("#" + id).val(feet + "'" + _inches + '"');
                            }
                        }
                        else {
                            var u = value[0];
                            var h = "";
                            var j = "";
                            var o = '"';
                            if (value.length == 2) { h = value[1]; }
                            if (value.length == 3) { h = value[1]; j = value[2];}
                            if (j != ""){o = j.concat(o);}
                            if (h != ""){o = h.concat(o);}
                            if (value.indexOf("'") == -1) { $("#" + id).val(u + "'"); if (o != '"') { $("#" + id).val($("#" + id).val() + o); } }
                        }
                    }
                }
                else {
                    if (id == "Weight")
                    {
                        if (convert) {
                            if (value.indexOf("kg") == -1) {
                                var e = parseInt(value.replace("lb", ""))
                                if (e > 1) {
                                    var newWeight = e * 0.453;
                                    $("#" + id).val(Math.round(newWeight) + "kg");
                                }
                                else {
                                    $("#" + id).val(e + "kg");
                                }
                            }
                        }
                        else {
                            if (value.indexOf("kg") == -1) { $("#" + id).val(value.replace("lb", "") + "kg"); }
                        }
                    }
                    if (id == "Height")
                    {
                        if (convert) {
                            if (value.indexOf("cm") == -1)
                            {
                                var g = value.replace("'", "").replace('"', '');
                                var q = g[0];
                                var w = "";
                                var r = "";
                                if (g.length > 1) { w = g[1]; }
                                if (g.length > 2) { r = g[2];}
                                inches = (parseInt(q) * 12)
                                if (w != "") { inches = (inches + parseInt(w.toString() + r.toString())); }
                                var cm = Math.round(inches * 2.54);
                                $("#" + id).val(cm + "cm");
                            }
                        }
                        else {
                            if (value.indexOf("cm") == -1) { $("#" + id).val(value.replace("\'", "").replace("\"", "") + "cm"); }
                        }
                    }
                }
            }
        },
        btnSwap: function (selected, unselected) {
            if (!$("#" + selected).hasClass("btn-primary")) {
                $("#" + selected).addClass("btn-primary");
                if ($("#" + selected).text() != "USA") { Imperial = true; }
                else { Imperial = false; }
            }
            $("#" + unselected).removeClass("btn-primary");
        },
        clearForm: function (){
            $("#Fname").val("");
            $("#Lname").val("");
            $("#PNumber").val("");
            $("#PNumber2").val("");
            $("#Email").val("");
            $("#Address").val("");
            $("#City").val("");
            $("#State").val("");
            $("#Zip").val("");
            $("#Height").val("");
            $("#Weight").val("");
            $("#Age").val("");
            $("#ski-brand").val("");
            $("#ski-size").val("");
            $("#ski-inventory").val("");
            $("#pole-brand").val("");
            $("#pole-size").val("");
            $("#pole-inventory").val("");
            $("#binding-brand").val("");
            $("#binding-size").val("");
            $("#binding-inventory").val("");
            $("#board-brand").val("");
            $("#board-size").val("");
            $("#board-inventory").val("");
            $("#boardbinding-brand").val("");
            $("#boardbinding-size").val("");
            $("#boardbinding-inventory").val("");
            $("#board-stance").val("");
            $("#stance-angles").val("");
            $("#boot-brand").val("");
            $("#boot-size").val("");
            $("#boot-inventory").val("");
            $("#boot-sole").val("");
            $("#customerNotes").val("");
            $("#swapR1").attr("disabled", "disabled");
        },
        createRental: function (set)
        {
            $("#finishUp").attr("disabled", "disabled");
            var skitype = function () {
                var inputs = $("[name='skiType']").toArray();
                var id = inputs.forEach(hasClass);
                var selected;
                function hasClass(item, index) {
                    if (item.getAttribute("class").toString().indexOf("btn-primary") !== -1) {
                        selected = item.getAttribute("id").toString();
                    }
                }
                switch(selected){
                    case "skiType1":
                        return "n\a";
                    case "skiType2":
                        return "1";
                    case "skiType3":
                        return "2";
                    case "skiType4":
                        return "3";
                }
            }
            var rental = {
                IdPrevious: $("#IdPrevious").val(),
                IdNext: $("#IdNext").val(),
                Address: $("#Address").val(),
                Age : $("#Age").val() == "" ? 0 : parseInt($("#Age").val()),
                BindingBrand : $("#binding-brand").val(),
                SnowboardBindingBrand : $("#boardbinding-brand").val(),
                BindingSize : $("#binding-size").val(),
                SnowboardBindingSize: $("#boardbinding-size").val(),
                BoardBrand : $("#board-brand").val(),
                BoardSize : $("#board-size").val(),
                BoardStance : $("#board-stance").val(),
                City : $("#City").val(),
                CustomerNotes : $("#customerNotes").val(),
                EmailAddress : $("#Email").val(),
                FirstName : $("#Fname").val(),
                Height : $("#Height").val(),
                LastName : $("#Lname").val(),
                PhoneNumber : $("#PNumber").val()==""? 0 : parseInt($("#PNumber").val()),
                PhoneNumber2: $("#PNumber2").val() == "" ? 0 : parseInt($("#PNumber2").val()),
                PoleBrand : $("#pole-brand").val(),
                PoleSize : $("#pole-size").val(),
                PostalCode : $("#Zip").val(),
                Sex : $("#Sex").val(),
                SkiBrand : $("#ski-brand").val(),
                SkiSize : $("#ski-size").val(),
                SkierType : skitype(),
                StanceAngles : $("#stance-angles").val(),
                State : $("#State").val(),
                Weight : $("#Weight").val() == "" ? 0 : parseInt($("#Weight").val()),
                BootSize : $("#boot-size").val(),
                BootBrand : $("#boot-brand").val(),
                DailyRental : $("#daily-rental").prop('checked'),
                BootSoleLength : $("#boot-sole").val(),
                SnowboardInventoryNumber: $("#board-inventory").val(),
                SkiInventoryNumber: $("#ski-inventory").val(),
                PoleInventoryNumber: $("#pole-inventory").val(),
                SkiBindingInventoryNumber: $("#binding-inventory").val(),
                SnowboardBindingInventoryNumber: $("#boardbinding-inventory").val(),
                BootsInventoryNumber: $("#boot-inventory").val(),
                IsMetric: Imperial,
                // DIN,
                DueDate : $("#due-date").val()
            }
            //console.log(rental);
            if (set==true) {
                postServerSet(rental);
            }
            else {
                if (set == "finish") { postServerSetFinal(rental); }
                else {
                    postServer(rental);
                }
            }
        },
        retrieveRental: function(id){
            $.get("Rentals/RetrieveRental", { id: parseInt(id) }, function (res) {
                $("#Fname").val(res.firstName);
                $("#Lname").val(res.lastName);
                $("#PNumber").val(res.phoneNumber);
                $("#PNumber2").val(res.phoneNumber2);
                $("#Email").val(res.emailAddress);
                $("#Address").val(res.address);
                $("#City").val(res.city);
                $("#State").val(res.state);
                $("#Zip").val(res.postalCode);
                $("#Height").val(res.height);
                $("#Weight").val(res.weight);
                $("#Age").val(res.age);
                $("#ski-brand").val(res.skiBrand);
                $("#ski-size").val(res.skiSize);
                $("#ski-inventory").val(res.skiInventoryNumber);
                $("#pole-brand").val(res.poleBrand);
                $("#pole-size").val(res.poleSize);
                $("#pole-inventory").val(res.poleInventoryNumber);
                $("#binding-brand").val(res.bindingBrand);
                $("#binding-size").val(res.bindingSize);
                $("#binding-inventory").val(res.bindingInventoryNumber);
                $("#board-brand").val(res.boardBrand);
                $("#board-size").val(res.boardSize);
                $("#board-inventory").val(res.boardInventoryNumber);
                $("#boardbinding-brand").val(res.snowboardBindingBrand);
                $("#boardbinding-size").val(res.snowboardBindingSize);
                $("#boardbinding-inventory").val(res.snowboardBindingInventoryNumber);
                $("#board-stance").val(res.boardStance);
                $("#stance-angles").val(res.stanceAngles);
                $("#boot-brand").val(res.bootBrand);
                $("#boot-size").val(res.bootSize);
                $("#boot-inventory").val(res.bootInventoryNumber);
                $("#boot-sole").val(res.bootSoleLength);
                $("#customerNotes").val(res.customerNotes);
            });
        },
        createSetMode: function (val) {
            $("#IdPrevious").val(val);
            $("#modal1").modal("show");
            $("#finishUp").text("Save and Finish");
            $("#finishUp").attr("onclick", "validation.createRental('finish')");
            $("#createSet2").text("Save and Continue");
            updateRentalSetCounter(true, 2);
            document.cookie = "SetCount=2";
            var inputs = $("#modal-system :input");
            for (var x = 0; x < inputs.length; x++) {
                let current = inputs[x];
                if (current.classList.contains("req-invalid")) { current.classList.remove("req-invalid"); }
                if (current.getAttribute("disabled") == "disabled") { current.removeAttribute("disabled"); }
            }
        }
        // End Public Namespace
        }
        
})();

// Attatch AutoComplete
$('.citySuggest').suggest(validation.citySuggest);
$('.skiSuggest').suggest(validation.skiBrands);
$("#delForm").tooltip({ trigger: "hover", title: "Clear Form", placement: "bottom" });