﻿var indexFunctions = {
    setup: function () {
        var disabled = false;
        if (sessionStorage.getItem('filter') != null) {
            document.getElementById('filterSelect').value = sessionStorage.getItem('filter');
        };
        var list = document.getElementsByClassName("daily");
        for (var x = 0; x < list.length; x++) {
            list[x].parentElement.parentElement.className = "DailyRental";
        }
        var list2 = document.getElementsByClassName("dailyOld");
        for (var x = 0; x < list2.length; x++) {
            list2[x].parentElement.parentElement.className = "DailyRentalOld";
        }

        var _selected = sessionStorage.getItem("FilterGroup");
        if (_selected != 'undefined') {
            if (_selected == "0") {
                document.getElementById('allFilter').classList.add("btn-selected");
                document.getElementById('TypeFilter').value = "0";
            }
            if (_selected == "1") {
                document.getElementById('seasonalFilter').classList.add("btn-selected");
                document.getElementById('TypeFilter').value = "1";
            }
            if (_selected == "2") {
                document.getElementById('dailyFilter').classList.add("btn-selected");
                document.getElementById('TypeFilter').value = "2";
            }
        }

        document.getElementById('filterSelect').addEventListener('click', function () {
            sessionStorage.setItem('filter', document.getElementById('filterSelect').value);
        })
        //add event listeners to the filter butotn group
        document.getElementById('allFilter').addEventListener("click", function () {
            document.getElementById("TypeFilter").value = "0";
            sessionStorage.setItem("FilterGroup", "0");
            document.getElementById("searchForm").submit();
            lockFilterGroup();
        });
        document.getElementById('seasonalFilter').addEventListener("click", function () {
            document.getElementById("TypeFilter").value = "1";
            sessionStorage.setItem("FilterGroup", "1");
            document.getElementById("searchForm").submit();
            lockFilterGroup();
        });
        document.getElementById('dailyFilter').addEventListener("click", function () {
            document.getElementById("TypeFilter").value = "2";
            sessionStorage.setItem("FilterGroup", "2");
            document.getElementById("searchForm").submit();
            lockFilterGroup();
        });
        var lockFilterGroup = () => { document.getElementById("typeButtonGroup").classList.add("noPointerEvents"); }
        var inBoxes = document.getElementsByClassName("checkLink");
        for (var x = 0; x < inBoxes.length; x++) {
            inBoxes[x].firstChild.attributes = "style:'pointer-events:none;'";
        }

        //Unused
        //set listener on return multiple set to activate when clicked
        //document.getElementById("returnMultipleStart").addEventListener("click", function () {
        //    document.getElementById("returnMultipleEnd").setAttribute("style", "color:red;");
        //    document.getElementById("returnMultipleStart").setAttribute("style", "display:none;");
        //    document.getElementById("returnMultipleEnd").addEventListener("click", function () { returnSet(); });
        //    var inBoxes = document.getElementsByClassName("checkContainer");
        //    for (var x = 0; x < inBoxes.length; x++) {
        //        inBoxes[x].firstChild.attributes = "style:'display:none;'";
        //    }
        //    console.log(inBoxes);
        //});

        // gather and submit data to server
        //function returnSet() {
        //    document.getElementById("returnMultipleEnd").setAttribute("style", "display:none");
        //    document.getElementById("returnMultipleStart").setAttribute("style", "");
        //}
    }
};
indexFunctions.setup();
